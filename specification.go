// Specification specifies the specification for a job.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package job

import (
	"bitbucket.org/pcas/irange"
	"bitbucket.org/pcas/job/errors"
	"math"
	"path/filepath"
	"strings"
	"time"
)

// Priority represents the priority of a job. Higher numbers represent higher priority.
type Priority uint8

// Named priority levels
const (
	Lowest     = Priority(0)
	Background = Priority(math.MaxUint8 / 4)
	Default    = Priority(math.MaxUint8 / 2)
	High       = Priority(3 * math.MaxUint8 / 4)
	Maximum    = Priority(math.MaxUint8)
)

// Specification represents a job as submitted by a client.
type Specification struct {
	// Name is the name of the job
	Name string
	// Manifest records the files and directories created on job submission
	Manifest Manifest
	// Script is the file in Manifest that is run for each task in the job
	Script string
	// WorkingDir is a directory in Manifest, either explicitly or implicitly (as a subpath)
	WorkingDir string
	// Metadata records user-provided metadata
	Metadata map[string]string
	// Range is the range of tasks that make up the job
	Range irange.Range
	// Priority is the priority level of the job
	Priority Priority
	// Lifetime is the maximum lifetime of a task
	Lifetime time.Duration
	// MaxRetries is the maximum number of times that a task is retried on error
	MaxRetries int
	// MaxConcurrency is the maximum number of simultaneously-active tasks for this job
	MaxConcurrency int
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// subpath returns true if and only if s is a rooted subpath of an element of M.
func subpath(s string, M Manifest) bool {
	// Make a slice containing the paths of files and directories
	files := M.Files()
	directories := M.Directories()
	paths := make([]string, len(files)+len(directories))
	copy(paths, files)
	paths = append(paths, directories...)
	// Check each path in turn
	for _, p := range paths {
		if extra, err := filepath.Rel(s, p); err == nil && !strings.HasPrefix(extra, "../") {
			return true
		}
	}
	return false
}

/////////////////////////////////////////////////////////////////////////
// Specification methods
/////////////////////////////////////////////////////////////////////////

// Validate validates the specification, returning an error if there is a problem.
func (S *Specification) Validate() error {
	if S == nil {
		return errors.ErrNilSpecification
	}
	// Names cannot begin or end with whitespace, or be empty
	if strings.TrimSpace(S.Name) != S.Name {
		return errors.ErrInvalidSpecification
	} else if S.Name == "" {
		return errors.ErrInvalidSpecification
	}
	// The Manifest must contain the Script. This also forces the Script to be a valid path, that is, a /-rooted path in canonical form.
	if !S.Manifest.ContainsFile(S.Script) {
		return errors.ErrInvalidSpecification
	}
	// The WorkingDir should be a valid path
	if err := validatePaths([]string{S.WorkingDir}); err != nil {
		return errors.ErrInvalidSpecification
	}
	// The WorkingDir should either be in the Manifest or a subpath of something in the Manifest
	if !subpath(S.WorkingDir, S.Manifest) {
		return errors.ErrInvalidSpecification
	}
	// The Range must be non-empty
	if S.Range.Len() == 0 {
		return errors.ErrInvalidSpecification
	}
	// Lifetime must be positive
	if S.Lifetime <= 0 {
		return errors.ErrInvalidSpecification
	}
	// MaxRetries must be non-negative
	if S.MaxRetries < 0 {
		return errors.ErrInvalidSpecification
	}
	// MaxConcurrency must be positive
	if S.MaxConcurrency <= 0 {
		return errors.ErrInvalidSpecification
	}
	// The specification is valid
	return nil
}

// IsEquivalentTo returns true if and only if S and T are equivalent.
func (S *Specification) IsEquivalentTo(T *Specification) (ok bool, err error) {
	if err = S.Validate(); err != nil {
		return
	} else if err = T.Validate(); err != nil {
		return
	}
	// Test Name equality
	if S.Name != T.Name {
		return
	}
	// Test equivalence of Manifests
	if !S.Manifest.IsEquivalentTo(T.Manifest) {
		return
	}
	// Test equality of Scripts and WorkingDirs
	if S.Script != T.Script {
		return
	} else if S.WorkingDir != T.WorkingDir {
		return
	}
	// Test equality of Metadata
	if len(S.Metadata) != len(T.Metadata) {
		return
	}
	for k, v := range S.Metadata {
		w, present := S.Metadata[k]
		if !present || v != w {
			return
		}
	}
	// Test equality of Ranges
	if S.Range.Len() != T.Range.Len() || irange.Diff(S.Range, T.Range).Len() != 0 {
		return
	}
	// Test equality of the remaining fields
	if S.Priority != T.Priority ||
		S.Lifetime != T.Lifetime ||
		S.MaxRetries != T.MaxRetries ||
		S.MaxConcurrency != T.MaxConcurrency {
		return
	}
	// S and T are equal
	return true, nil
}
