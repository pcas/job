module bitbucket.org/pcas/job

go 1.15

require (
	bitbucket.org/pcas/fs v0.1.12
	bitbucket.org/pcas/golua v0.1.5
	bitbucket.org/pcas/irange v0.0.10
	bitbucket.org/pcas/keyvalue v0.1.31
	bitbucket.org/pcas/logger v0.1.33
	bitbucket.org/pcas/lua v0.1.14
	bitbucket.org/pcas/metrics v0.1.25
	bitbucket.org/pcas/sslflag v0.0.13
	bitbucket.org/pcastools/address v0.1.3
	bitbucket.org/pcastools/bytesbuffer v1.0.2
	bitbucket.org/pcastools/cleanup v1.0.3
	bitbucket.org/pcastools/contextutil v1.0.2
	bitbucket.org/pcastools/convert v1.0.4
	bitbucket.org/pcastools/fatal v1.0.2
	bitbucket.org/pcastools/flag v0.0.17
	bitbucket.org/pcastools/grpcutil v1.0.11
	bitbucket.org/pcastools/listenutil v0.0.9
	bitbucket.org/pcastools/log v1.0.3
	bitbucket.org/pcastools/timeutil v0.1.2
	bitbucket.org/pcastools/ulid v0.1.4
	bitbucket.org/pcastools/version v0.0.4
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/stretchr/testify v1.7.0
	google.golang.org/grpc v1.44.0
	google.golang.org/protobuf v1.27.1
)
