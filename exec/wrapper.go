// Wrapper provides a wrapper that converts a job.Task into a joblib.Task.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package exec

import (
	"bitbucket.org/pcas/job"
	"bitbucket.org/pcas/lua/lib/joblib"
	"bitbucket.org/pcastools/ulid"
)

// wrapper wraps a job.Task, converting it to a joblua.Task
type wrapper struct {
	job.Task // The wrapped task
}

// wrapper satisfies joblib.Task
var _ joblib.Task = &wrapper{}

/////////////////////////////////////////////////////////////////////////
// wrapper functions
/////////////////////////////////////////////////////////////////////////

// Manifest returns the manifest for the job of which this task is a part.
func (w *wrapper) Manifest() joblib.Manifest {
	return w.Task.Manifest()
}

// ID returns the ID of the task
func (w *wrapper) ID() ulid.ULID {
	return ulid.ULID(w.Task.ID())
}

// JobID returns the ID of the job of which this task forms a part.
func (w *wrapper) JobID() ulid.ULID {
	return ulid.ULID(w.Task.JobID())
}

// Priority returns the priority for the job of which this task forms a part.
func (w *wrapper) Priority() uint8 {
	return uint8(w.Task.Priority())
}

// wrap returns a joblib.Task defined by t
func wrap(t job.Task) joblib.Task {
	return &wrapper{Task: t}
}
