// Jobtest provides tests for implementations of job.SubmitterStorage and job.WorkerStorage.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package jobtest

import (
	"bitbucket.org/pcas/irange"
	"bitbucket.org/pcas/job"
	jerrors "bitbucket.org/pcas/job/errors"
	"bitbucket.org/pcas/job/internal/spectest"
	"bitbucket.org/pcastools/bytesbuffer"
	"bitbucket.org/pcastools/ulid"
	"context"
	"github.com/stretchr/testify/require"
	"io"
	"math/rand"
	"os"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// wrap wraps a test function so that it can be passed to testing.T.Run.
func wrap(ctx context.Context, s job.Storage, f func(context.Context, job.Storage, *require.Assertions)) func(*testing.T) {
	return func(t *testing.T) {
		f(ctx, s, require.New(t))
	}
}

// randomBytes returns an io.Reader containing 1024 random bytes
func randomBytes() io.Reader {
	b := make([]byte, 1024)
	_, _ = rand.Read(b) // the return values are always len(b) and nil
	return bytesbuffer.NewBytes(b)
}

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// testInvalidName tests that unknown job IDs are handled correctly.
func testInvalidName(ctx context.Context, S job.Storage, require *require.Assertions) {
	// We only test the SubmitterStorage part of the interface
	s := S.(job.SubmitterStorage)
	// Make a new (and hence unknown) Job ID
	u, err := ulid.New()
	require.NoError(err)
	j := job.ID(u)
	// The nil ID should also be unknown
	badIDs := []job.ID{j, job.NilID}
	for _, id := range badIDs {
		// test Status
		_, err := s.Status(ctx, id)
		require.Equal(jerrors.ErrUnknownJob, err)
		// test Info
		_, err = s.Info(ctx, id, 0)
		require.Equal(jerrors.ErrUnknownJob, err)
		// test Delete
		err = s.Delete(ctx, id)
		require.Equal(jerrors.ErrUnknownJob, err)
		// test Describe
		_, err = s.Describe(ctx, id)
		require.Equal(jerrors.ErrUnknownJob, err)
	}
}

// testClientStorage tests Submit, Upload, Status, Info, List, ListWithPriority, Delete, and Describe.
func testClientStorage(ctx context.Context, S job.Storage, require *require.Assertions) {
	// We only test the ClientStorage part of the interface
	s := S.(job.SubmitterStorage)
	// Test invalid specifications in Submit
	for _, spec := range spectest.InvalidSpecs() {
		_, err := s.Submit(ctx, spec)
		require.Equal(jerrors.ErrInvalidSpecification, err)
	}
	// Submitting a valid specification should succeed
	spec := spectest.ValidSpec("")
	id, err := s.Submit(ctx, spec)
	require.NoError(err)
	// Uploading a file that is not in the manifest should fail
	require.Equal(jerrors.ErrUnknownFile, s.Upload(ctx, id, "/totally/irrelevant/path", randomBytes()))
	// Uploading files in the manifest should succeed
	for _, p := range spec.Manifest.Files() {
		require.NoError(s.Upload(ctx, id, p, randomBytes()))
	}
	// Calling Describe should give the same specification back
	x, err := s.Describe(ctx, id)
	require.NoError(err)
	ok, err := spec.IsEquivalentTo(x)
	require.NoError(err)
	require.True(ok)
	// Status should show all the tasks as pending
	st, err := s.Status(ctx, id)
	require.NoError(err)
	require.True(st.Pending().IsEqualTo(irange.Interval(100, 200)))
	require.True(st.Active().IsEmpty())
	require.True(st.Succeeded().IsEmpty())
	require.True(st.Failed().IsEmpty())
	// Info should also show tasks as pending
	inf, err := s.Info(ctx, id, 134)
	require.NoError(err)
	require.Equal(job.Pending, inf.State())
	require.Equal(0, inf.Failures())
	// Non-existent tasks should cause Info to fail
	_, err = s.Info(ctx, id, 234)
	require.Equal(jerrors.ErrNotInRange, err)
	// List should show precisely this job
	l, err := s.List(ctx)
	require.NoError(err)
	require.Equal([]job.ID{id}, l)
	// ListWithPriority should show either precisely this job or nothing
	l, err = s.ListWithPriority(ctx, 0)
	require.NoError(err)
	require.Equal([]job.ID{id}, l)
	l, err = s.ListWithPriority(ctx, 7)
	require.NoError(err)
	require.Empty(l)
	// Deleting the job should succeed
	require.NoError(s.Delete(ctx, id))
	// Now there should be no jobs
	l, err = s.List(ctx)
	require.NoError(err)
	require.Empty(l)
}

// testWorkerStorage tests Next, Success, Error, Fatal, Requeue, and Heartbeat.
func testWorkerStorage(ctx context.Context, S job.Storage, require *require.Assertions) {
	s := S.(job.SubmitterStorage)
	w := S.(job.WorkerStorage)
	// We start with no jobs
	l, err := s.List(ctx)
	require.NoError(err)
	require.Empty(l)
	// Create a job
	spec := spectest.ValidSpec("")
	id, err := s.Submit(ctx, spec)
	require.NoError(err)
	// Upload the files
	for _, p := range spec.Manifest.Files() {
		require.NoError(s.Upload(ctx, id, p, randomBytes()))
	}
	// Create some worker metadata
	h, err := os.Hostname()
	require.NoError(err)
	mt := job.Metadata{
		Hostname: h,
		AppName:  "testWorker",
	}
	// Grab four tasks
	Ts := make([]job.Task, 0, 4)
	for i := 0; i < 4; i++ {
		t, err := w.Next(ctx, mt)
		require.NoError(err)
		Ts = append(Ts, t)
	}
	// Right now there should be 97 pending tasks, 0 failed or succeeded tasks, and the correct active tasks
	st, err := s.Status(ctx, id)
	require.NoError(err)
	require.True(st.Succeeded().IsEmpty())
	require.True(st.Failed().IsEmpty())
	require.Equal(97, st.Pending().Len())
	R := irange.Empty
	for _, t := range Ts {
		R = R.Include(t.Value())
	}
	require.True(st.Active().IsEqualTo(R))
	// Make a heartbeat
	for _, t := range Ts {
		ok, err := w.Heartbeat(ctx, t)
		require.NoError(err)
		require.True(ok)
	}
	// Mark the tasks as finished in various ways
	require.NoError(w.Success(ctx, Ts[0]))
	require.NoError(w.Error(ctx, Ts[1]))
	require.NoError(w.Requeue(ctx, Ts[2]))
	require.NoError(w.Fatal(ctx, Ts[3]))
	// Check the updated status
	st, err = s.Status(ctx, id)
	require.NoError(err)
	n := Ts[0].Value()
	m := Ts[3].Value()
	require.True(st.Succeeded().IsEqualTo(irange.Interval(n, n)))
	require.True(st.Failed().IsEqualTo(irange.Interval(m, m)))
	R = irange.Interval(100, 200)
	for _, x := range []int64{n, m} {
		R, err = R.Exclude(x)
		require.NoError(err)
	}
	require.True(st.Pending().IsEqualTo(R))
	// Heartbeart again. Despite the fact that these tasks are complete, Heartbeat should still return true.
	for _, t := range Ts {
		ok, err := w.Heartbeat(ctx, t)
		require.NoError(err)
		require.True(ok)
	}
	// A nil Heartbeat should succeed
	ok, err := w.Heartbeat(ctx, nil)
	require.NoError(err)
	require.True(ok)
	// Grab a couple more tasks
	Ts = make([]job.Task, 0, 2)
	for i := 0; i < 2; i++ {
		t, err := w.Next(ctx, mt)
		require.NoError(err)
		Ts = append(Ts, t)
	}
	// Delete the job
	require.NoError(s.Delete(ctx, id))
	// Check that the tasks got marked for cancellation
	for _, t := range Ts {
		ok, err := w.Heartbeat(ctx, t)
		require.NoError(err)
		require.False(ok)
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Run runs the standard tests on the job storage system s.
func Run(ctx context.Context, s job.Storage, t *testing.T) {
	// Test that invalid names are handled properly
	t.Run("TestInvalidName", wrap(ctx, s, testInvalidName))
	// Test Submit, Status, Info, List, ListWithPriority, Delete, and Describe
	t.Run("TestClient", wrap(ctx, s, testClientStorage))
	// Test Next, Success, Error, Fatal, Requeue, and Heartbeat
	t.Run("TestWorker", wrap(ctx, s, testWorkerStorage))
}
