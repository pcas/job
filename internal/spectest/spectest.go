// Spectest defines helper functions for testing Specification validation.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package spectest

import (
	"bitbucket.org/pcas/irange"
	"bitbucket.org/pcas/job"
	"time"

	_ "crypto/sha1" // make sure we have the SHA1 hash
)

// ValidSpec returns a valid specification with files based at root.
func ValidSpec(root string) *job.Specification {
	paths := []string{root + "/foo/bar/baz", root + "/fee/fi/fo", root + "/fum"}
	directories := []string{root + "/apple/pear", root + "/stairs"}
	manifest, err := job.NewManifest(paths, directories)
	if err != nil {
		panic(err) // Cannot happen
	}
	return &job.Specification{
		Name:           "test specification",
		Manifest:       manifest,
		Script:         root + "/fum",
		WorkingDir:     root + "/fee/fi",
		Metadata:       nil,
		Range:          irange.Interval(100, 200),
		Priority:       0,
		Lifetime:       1 * time.Hour,
		MaxRetries:     3,
		MaxConcurrency: 100,
	}
}

// InvalidSpecs returns a slice of non-nil, invalid *job.Specifications.
func InvalidSpecs() []*job.Specification {
	invalid := make([]*job.Specification, 0)
	// Names must be valid
	spec := ValidSpec("")
	for _, name := range []string{"", " fish and chips", "fish and chips\t", "\t \t"} {
		spec.Name = name
		invalid = append(invalid, spec)
	}
	// Scripts must be valid paths
	invalidPaths := []string{"unrooted", "", ".", "/unclean//path", "/another/../unclean/../path"}
	spec = ValidSpec("")
	for _, p := range invalidPaths {
		spec.Script = p
		invalid = append(invalid, spec)
	}
	// The Script must be in the Manifest
	spec = ValidSpec("")
	spec.Script = "/valid/but/unrelated/path"
	invalid = append(invalid, spec)
	// WorkingDir must be a valid path
	spec = ValidSpec("")
	for _, p := range invalidPaths {
		spec.WorkingDir = p
		invalid = append(invalid, spec)
	}
	// The WorkingDir must be in the Manifest, at least implicitly
	spec = ValidSpec("")
	spec.WorkingDir = "/valid/but/unrelated/path"
	invalid = append(invalid, spec)
	// The Range must be non-empty
	spec = ValidSpec("")
	spec.Range = irange.Empty
	invalid = append(invalid, spec)
	// The Lifetime must be positive
	spec = ValidSpec("")
	for _, d := range []time.Duration{0 * time.Second, -1 * time.Second} {
		spec.Lifetime = d
		invalid = append(invalid, spec)
	}
	// MaxRetries can't be negative
	spec.MaxRetries = -1
	invalid = append(invalid, spec)
	// MaxConcurrency must be positive
	spec = ValidSpec("")
	for _, m := range []int{0, -1} {
		spec.MaxConcurrency = m
		invalid = append(invalid, spec)
	}
	return invalid
}
