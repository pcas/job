// Specification_test provides tests for the Validate method of *job.Specification objects.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package spectest

import (
	"bitbucket.org/pcas/job"
	jerrors "bitbucket.org/pcas/job/errors"
	"github.com/stretchr/testify/require"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Tests
/////////////////////////////////////////////////////////////////////////

// TestValidate tests the Validate method of *job.Specifiation objects.
func TestValidate(t *testing.T) {
	require := require.New(t)
	// Nil specifications are invalid
	var spec *job.Specification
	require.Equal(jerrors.ErrNilSpecification, spec.Validate())
	// A valid specification validates correctly
	spec = ValidSpec("/some/root/path")
	require.NoError(spec.Validate())
	// Invalid specifications validate correctly
	for _, spec := range InvalidSpecs() {
		require.Equal(jerrors.ErrInvalidSpecification, spec.Validate())
	}
	// MaxRetries may be zero
	spec = ValidSpec("/some/root/path")
	spec.MaxRetries = 0
	require.NoError(spec.Validate())
}
