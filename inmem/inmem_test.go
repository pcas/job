// Inmem_test provides tests for the inmem package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package inmem

import (
	"context"
	"io"
	"io/ioutil"
	"os"
	"testing"
	"time"

	"bitbucket.org/pcas/fs"
	"bitbucket.org/pcas/fs/filesystem"
	"bitbucket.org/pcas/irange/inmem"
	"bitbucket.org/pcas/job"
	"bitbucket.org/pcas/job/internal/jobtest"
	"bitbucket.org/pcas/job/internal/spectest"
	"bitbucket.org/pcastools/bytesbuffer"
	"bitbucket.org/pcastools/ulid"
	"github.com/stretchr/testify/require"
)

// testNilClosed tests that nil and closed job storage systems behave correctly. S should be closed.
func testNilClosed(ctx context.Context, S job.Storage, t *testing.T) {
	require := require.New(t)
	// Make a submitter and worker view of the closed storage system
	s := S.(job.SubmitterStorage)
	w := S.(job.WorkerStorage)
	// Make a client and worker view of a nil storage system
	var nilS *scheduler
	var nilC job.SubmitterStorage = nilS
	var nilW job.WorkerStorage = nilS
	// Make the ID, spec, and metadata we need
	u, err := ulid.New()
	require.NoError(err)
	id := job.ID(u)
	spec := spectest.ValidSpec("")
	h, err := os.Hostname()
	mt := job.Metadata{
		AppName:  "inmem test",
		Hostname: h,
	}
	// Test the nil Client
	_, err = nilC.Submit(ctx, spec)
	require.Equal(ErrNilScheduler, err)
	_, err = nilC.Status(ctx, id)
	require.Equal(ErrNilScheduler, err)
	_, err = nilC.Info(ctx, id, 0)
	require.Equal(ErrNilScheduler, err)
	_, err = nilC.List(ctx)
	require.Equal(ErrNilScheduler, err)
	_, err = nilC.ListWithPriority(ctx, 7)
	require.Equal(ErrNilScheduler, err)
	err = nilC.Delete(ctx, id)
	require.Equal(ErrNilScheduler, err)
	_, err = nilC.Describe(ctx, id)
	require.Equal(ErrNilScheduler, err)
	// Test the nil worker
	_, err = nilW.Next(ctx, mt)
	require.Equal(ErrNilScheduler, err)
	err = nilW.Success(ctx, nil)
	require.Equal(ErrNilScheduler, err)
	err = nilW.Error(ctx, nil)
	require.Equal(ErrNilScheduler, err)
	err = nilW.Requeue(ctx, nil)
	require.Equal(ErrNilScheduler, err)
	err = nilW.Fatal(ctx, nil)
	require.Equal(ErrNilScheduler, err)
	_, err = nilW.Heartbeat(ctx, nil)
	require.Equal(ErrNilScheduler, err)
	// Test a closed Client
	_, err = s.Submit(ctx, spec)
	require.Equal(ErrClosed, err)
	_, err = s.Status(ctx, id)
	require.Equal(ErrClosed, err)
	_, err = s.Info(ctx, id, 0)
	require.Equal(ErrClosed, err)
	_, err = s.List(ctx)
	require.Equal(ErrClosed, err)
	_, err = s.ListWithPriority(ctx, 7)
	require.Equal(ErrClosed, err)
	err = s.Delete(ctx, id)
	require.Equal(ErrClosed, err)
	_, err = s.Describe(ctx, id)
	require.Equal(ErrClosed, err)
	// Test a closed worker
	_, err = w.Next(ctx, mt)
	require.Equal(ErrClosed, err)
	err = w.Success(ctx, nil)
	require.Equal(ErrClosed, err)
	err = w.Error(ctx, nil)
	require.Equal(ErrClosed, err)
	err = w.Requeue(ctx, nil)
	require.Equal(ErrClosed, err)
	err = w.Fatal(ctx, nil)
	require.Equal(ErrClosed, err)
	_, err = w.Heartbeat(ctx, nil)
	require.Equal(ErrClosed, err)
}

// testCleanup tests the cleanup logic for inmem job storage.
func testCleanup(ctx context.Context, S job.Storage, t *testing.T) {
	require := require.New(t)
	// Submit a job
	spec := spectest.ValidSpec("")
	id, err := S.Submit(ctx, spec)
	require.NoError(err)
	b := bytesbuffer.NewString("example content for manifest files")
	for _, p := range spec.Manifest.Files() {
		require.NoError(S.Upload(ctx, id, p, b))
	}
	// Recover S as an inmem instance
	s, ok := S.(*scheduler)
	require.True(ok)
	// At this point the job directory should exist
	ok, err = fs.Exists(ctx, s.fs, s.jobDir(id))
	require.NoError(err)
	require.True(ok)
	// Delete the job
	require.NoError(S.Delete(ctx, id))
	// Check that the job directory was deleted
	ok, err = fs.Exists(ctx, s.fs, s.jobDir(id))
	require.NoError(err)
	require.False(ok)
}

func TestAll(t *testing.T) {
	// Create a context with reasonable timeout for the tests to run
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	// Create some temporary storage
	baseDir, err := ioutil.TempDir(os.TempDir(), "fs_*")
	require.NoError(t, err)
	fs, err := filesystem.New(baseDir)
	require.NoError(t, err)
	defer os.RemoveAll(baseDir)
	// Create the scheduler
	cfg := &Config{
		Config: inmem.DefaultConfig(),
		Fs:     fs,
	}
	s, err := New(ctx, cfg)
	require.NoError(t, err)
	// Run the tests
	jobtest.Run(ctx, s, t)
	testCleanup(ctx, s, t)
	// Test that nil and closed clients and workers behave sensibly.
	s.(io.Closer).Close()
	testNilClosed(ctx, s, t)
}
