// Jobtask defines data types representing jobs and tasks.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package inmem

import (
	"bitbucket.org/pcas/irange"
	"bitbucket.org/pcas/job"
	"bitbucket.org/pcas/job/errors"
	"time"
)

// jb represents a job from the point of view of the server.
type jb struct {
	// Name is the name of the job
	Name string
	// Manifest records the files and directories created on job submission
	Manifest job.Manifest
	// Script is the file in Manifest that is run for each task in the job
	Script string
	// WorkingDir is a directory in Manifest, either explicitly or implicitly (as a subpath)
	WorkingDir string
	// Metadata records user-provided metadata
	Metadata map[string]string
	// Priority is the priority level of the job
	Priority job.Priority
	// id is an ID that identifies the job
	ID job.ID
}

// tsk represents a task.
type tsk struct {
	j *jb          // the job of which this task is a part
	e irange.Entry // the entry corresponding to this task in the range corresponding to this job
}

// tsk satisfies the job.Task interface
var _ job.Task = &tsk{}

/////////////////////////////////////////////////////////////////////////
// Jb functions
/////////////////////////////////////////////////////////////////////////

// newJb returns the job with the given specification and ID.
func newJb(spec *job.Specification, id job.ID) (*jb, error) {
	if spec == nil {
		return nil, errors.ErrNilSpecification
	}
	return &jb{
		Name:       spec.Name,
		Manifest:   spec.Manifest,
		Script:     spec.Script,
		WorkingDir: spec.WorkingDir,
		Metadata:   spec.Metadata,
		Priority:   spec.Priority,
		ID:         id,
	}, nil
}

/////////////////////////////////////////////////////////////////////////
// Tsk functions
/////////////////////////////////////////////////////////////////////////

// Name is the name of the job to which this task belongs.
func (t *tsk) Name() string {
	return t.j.Name
}

// JobID is the ID of the job to which this task belongs.
func (t *tsk) JobID() job.ID {
	return t.j.ID
}

// Manifest records the files and directories created on job submission.
func (t *tsk) Manifest() job.Manifest {
	return t.j.Manifest
}

// Script is the file in Manifest that is run for each task in the job.
func (t *tsk) Script() string {
	return t.j.Script
}

// WorkingDir is a directory in Manifest, either explicitly or implicitly (as a subpath).
func (t *tsk) WorkingDir() string {
	return t.j.WorkingDir
}

// Metadata records user-provided metadata.
func (t *tsk) Metadata() map[string]string {
	// Return a copy
	x := make(map[string]string, len(t.j.Metadata))
	for k, v := range t.j.Metadata {
		x[k] = v
	}
	return x
}

// Priority is the priority level of the job.
func (t *tsk) Priority() job.Priority {
	return t.j.Priority
}

// ID is the ID of the task.
func (t *tsk) ID() job.TaskID {
	return job.TaskID(t.e.ID())
}

// Value is the value of the task.
func (t *tsk) Value() int64 {
	return t.e.Value()
}

// Deadline is the time at which the task will go stale.
func (t *tsk) Deadline() time.Time {
	return t.e.Deadline()
}

// Failures is the number of times that this task has failed.
func (t *tsk) Failures() int {
	return t.e.Failures()
}
