// Config handles configuration for the in-memory job server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package inmem

import (
	"bitbucket.org/pcas/fs"
	ir "bitbucket.org/pcas/irange/inmem"
	"errors"
	"sync"
)

// Config describes the configuration options we allow a user to set on an inmem instance.
type Config struct {
	*ir.Config
	Fs fs.Interface // the fs storage on which files and directories in the manifest live
}

// The default values for the client configuration, along with controlling mutex. The initial default values are assigned at init.
var (
	defaultsm sync.Mutex
	defaults  *Config
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init sets the initial default values.
func init() {
	defaults = &Config{
		Config: ir.DefaultConfig(),
	}
}

/////////////////////////////////////////////////////////////////////////
// Config functions
/////////////////////////////////////////////////////////////////////////

// DefaultConfig returns a new client configuration initialised with the default values.
func DefaultConfig() *Config {
	defaultsm.Lock()
	defer defaultsm.Unlock()
	return defaults.Copy()
}

// SetDefaultConfig sets the default configuration to c and returns the old default configuration. This change will be reflected in future calls to DefaultConfig.
func SetDefaultConfig(c *Config) *Config {
	defaultsm.Lock()
	defer defaultsm.Unlock()
	oldc := defaults
	defaults = c.Copy()
	return oldc
}

// Validate validates the configuration, returning an error if there's a problem.
func (c *Config) Validate() error {
	if c == nil {
		return errors.New("illegal nil configuration")
	} else if c.Fs == nil {
		return errors.New("illegal nil storage")
	}
	return c.Config.Validate()
}

// Copy returns a copy of the configuration.
func (c *Config) Copy() *Config {
	return &Config{
		Config: c.Config.Copy(),
		Fs:     c.Fs,
	}
}
