// Info defines a type satisfying job.Info that wraps an irange.Info.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package inmem

import (
	"bitbucket.org/pcas/irange"
	"bitbucket.org/pcas/job"
)

// wrapInfo is a wrapper that turns an irange.Info into a job.Info
type wrapInfo struct {
	irange.Info
}

var _ job.Info = wrapInfo{}

// State returns the state of this entry.
func (w wrapInfo) State() job.State {
	return job.State(w.Info.State())
}
