// Worker defines the scheduleWorker that maintains a scheduler object.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package inmem

import (
	"bitbucket.org/pcas/fs"
	"bitbucket.org/pcas/irange"
	irerrors "bitbucket.org/pcas/irange/errors"
	"bitbucket.org/pcas/job"
	jerrors "bitbucket.org/pcas/job/errors"
	"bitbucket.org/pcastools/contextutil"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/ulid"
	"context"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"sync"
	"time"
)

// requestCode specifies the type of request to the worker.
type requestCode uint8

// The request codes
const (
	// SubmitterStorage Requests
	SubmitReq = iota
	SubmitFileReq
	StatusReq
	InfoReq
	ListReq
	ListWithPriorityReq
	DeleteReq
	DescribeReq

	// WorkerStorage requests. Note that Next is not implemented via a request. Instead the next task is read from the scheduler channel
	RetrieveFileReq
	SuccessReq
	ErrorReq
	RequeueReq
	FatalReq
	HeartbeatReq

	// ActivateReq marks a task as active
	ActivateReq
)

// request describes a request to the scheduleWorker
type request struct {
	code         requestCode        // the type of request
	id           job.ID             // the job ID, for SubmitFile, Status, Info, Delete, and Describe requests
	manifestPath string             // the path as in the manifest, for SubmitFile and RetrieveFile requests
	path         string             // the path on fs storage, for SubmitFile requests
	spec         *job.Specification // the job specification, for Submit requests
	value        int64              // the task value, for Info requests
	level        job.Priority       // the priority level, for ListWithPriority requests
	task         job.Task           // the task, for Submit, Error, Requeue, Fatal, Heartbeat, and Activate requests
	doneC        chan struct{}      // the requestor closing this channel signals cancellation of the request
}

// response describes a response from the scheduleWorker
type response struct {
	err     error
	id      job.ID             // for Submit requests
	status  job.Status         // for Status requests
	info    job.Info           // for Info requests
	jobIDs  []job.ID           // for List and ListWithPriority requests
	spec    *job.Specification // for Describe requests
	path    string             // for RetrieveFile requests
	carryOn bool               // for Heartbeat requests
}

// init seeds the random number generator
func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// weight is the weighting function used to determine the probability that a job of priority p will run. It maps the zero priority to 1, and all other priorities to themselves.
func weight(p job.Priority) int {
	x := int(p)
	if x == 0 {
		return 1
	}
	return x
}

// wrap wraps err in a *response object
func wrap(err error) *response {
	return &response{err: err}
}

// rangeID returns the irange.ID corresponding to the given task.
func rangeID(t job.Task) irange.ID {
	return irange.ID(t.ID())
}

// fanIn returns a channel outC down which at most one job will be passed, and a channel doneC which can be closed to cancel the request for jobs. Any job passed down outC will have at least one task available. The channel blockC will be closed after a job is available on outC or after cancellation is complete. Errors are logged to lg but otherwise ignored. To avoid race conditions, no task processing should occur until blockC closes.
func (s *scheduler) fanIn() (outC chan *jb, doneC chan struct{}, blockC chan struct{}) {
	lg := log.PrefixWith(s.Log(), "[fanIn]")
	// Make the channels
	outC = make(chan *jb)
	doneC = make(chan struct{})
	blockC = make(chan struct{})
	// Make a context for cancellation
	ctx, cancel := context.WithCancel(context.Background())
	// listenC is the input channel for the listener and the output channel for the workers
	listenC := make(chan *jb)
	// wDoneC is the done channel for the workers.
	wDoneC := make(chan struct{})
	// Make worker metadata
	hostname, err := os.Hostname()
	if err != nil {
		lg.Printf("error getting hostname: %v", err)
		hostname = "unknown"
	}
	m := &irange.Metadata{
		AppName:  "inmem scheduler",
		Hostname: hostname,
	}
	// We launch one worker per job. To shut down a worker, one needs to cancel the context and close wDoneC.
	var wg sync.WaitGroup
	wg.Add(len(s.jobs))
	for _, j := range s.jobs {
		go func(j *jb) {
			defer wg.Done()
			// Try to get a task from this job
			e, err := s.S.Next(ctx, j.ID.String(), m)
			if err != nil {
				if err != context.Canceled {
					lg.Printf("error getting Next value for job %v: %v", j.ID.String(), err)
				}
				return
			}
			// Requeue the task, taking care that this doesn't get cancelled by ctx
			if err := s.S.Requeue(context.Background(), e.ID()); err != nil {
				lg.Printf("error requeueing task %v for job %v: %v", e.ID(), j.ID.String(), err)
				return
			}
			// Cancel all the other goroutines
			cancel()
			// Try to push j down the output channel
			select {
			case listenC <- j:
			case <-wDoneC:
			}
		}(j)
	}
	// Launch the listener
	go func() {
		var j *jb
		var hasJob bool
		// Wait for a job, or for cancellation
		select {
		case j = <-listenC:
			hasJob = true
		case <-doneC:
		}
		// Shut down the workers
		cancel()
		close(wDoneC)
		// Try to push j down the output channel
		if hasJob {
			select {
			case outC <- j:
			case <-doneC:
			}
		}
		// Clean up
		wg.Wait()
		close(outC)
		close(listenC)
		close(blockC)
	}()
	// Return the channels
	return outC, doneC, blockC
}

/////////////////////////////////////////////////////////////////////////
// worker methods
/////////////////////////////////////////////////////////////////////////

// nextJob returns true followed by a random job that has tasks available, weighted by priority. If there is no such job, returns false, nil. This should only be called from scheduleWorker. Errors are logged but otherwise ignored.
func (s *scheduler) nextJob() (bool, *jb) {
	lg := log.PrefixWith(s.Log(), "[nextJob]")
	// avail and weights are parallel sequences of available jobs and their weights
	avail := make([]*jb, 0, len(s.jobs))
	weights := make([]int, 0, len(s.jobs))
	var totalWeight int
	// Record the available jobs and their weights
	for _, j := range s.jobs {
		st, err := s.S.Status(context.Background(), j.ID.String())
		if err != nil {
			// Log the error and move on
			lg.Printf("error getting status for job %v: %v", j.ID.String(), err)
			continue
		}
		// Check if there are any available tasks
		if st.Pending().Len() > 0 {
			avail = append(avail, j)
			w := weight(j.Priority)
			weights = append(weights, w)
			totalWeight += w
		}
	}
	// Are there any jobs with available tasks?
	if len(avail) == 0 {
		return false, nil
	}
	// Return a random job. Since weights are positive, so is totalWeight.
	x := rand.Intn(totalWeight)
	for i, w := range weights {
		if x < w {
			return true, avail[i]
		}
		x -= w
	}
	panic(fatal.ImpossibleToGetHere()) // never happens
}

// pruneStale removes stale entries from the map that records tasks that should be cancelled. Should only be called from scheduleWorker.
func (s *scheduler) pruneStale() {
	now := time.Now()
	// Find the stale entries
	stale := make([]job.TaskID, 0, len(s.toCancel))
	for id, t := range s.toCancel {
		if t.Before(now) {
			stale = append(stale, id)
		}
	}
	// Delete the stale entries
	for _, id := range stale {
		delete(s.toCancel, id)
	}
}

// prunePending removes stale entries from the map that records pending jobs that should be cancelled. Should only be called from scheduleWorker.
func (s *scheduler) prunePending() {
	now := time.Now()
	// Find the stale entries
	stale := make([]job.ID, 0, len(s.cancelPending))
	for id, t := range s.cancelPending {
		if t.Before(now) {
			stale = append(stale, id)
		}
	}
	// Delete the stale entries
	for _, id := range stale {
		delete(s.pending, id)
		delete(s.pendingSpecs, id)
		delete(s.files, id)
		delete(s.cancelPending, id)
	}
}

// processTask processes the task t according to the request code c
func (s *scheduler) processTask(ctx context.Context, t job.Task, c requestCode) error {
	lg := log.PrefixWith(s.Log(), "[processTask]")
	// Sanity check
	if t == nil {
		lg.Printf("got nil Task")
		return jerrors.ErrNilTask
	}
	// Mark the task as inactive
	delete(s.tasks[t.JobID()], t.ID())
	// Inform the range server
	var err error
	switch c {
	case SuccessReq:
		err = s.S.Success(ctx, rangeID(t))
	case ErrorReq:
		err = s.S.Error(ctx, rangeID(t))
	case RequeueReq:
		err = s.S.Requeue(ctx, rangeID(t))
	case FatalReq:
		err = s.S.Fatal(ctx, rangeID(t))
	default:
		lg.Printf("invalid request code: %v", c)
		err = fmt.Errorf("invalid request code: %v", c)
	}
	if err != nil {
		lg.Printf("Got error for task %v: %v", t, err)
		return err
	}
	if c == RequeueReq {
		// The job cannot be complete, so we return
		return nil
	}
	// Check if the job is complete
	id := t.JobID()
	st, err := s.S.Status(ctx, id.String())
	if err != nil {
		lg.Printf("Error getting status: %v", err)
		return err
	}
	if st.Active().IsEmpty() && st.Pending().IsEmpty() {
		lg.Printf("Job %s is now complete.", id)
		return s.deleteJob(ctx, id)
	}
	return nil
}

// deleteJob attempts to delete the job with the given ID, returning jerrors.ErrUnknownJob if there is no such job
func (s *scheduler) deleteJob(ctx context.Context, id job.ID) error {
	lg := log.PrefixWith(s.Log(), "[deleteJob]")
	lg.Printf("Attempting to delete job %v", id.String())
	// Mark the active tasks as to be cancelled
	Ts, ok := s.tasks[id]
	if !ok {
		lg.Printf("Error: unknown job")
		return jerrors.ErrUnknownJob
	}
	for _, t := range Ts {
		lg.Printf("Marking task %v for cancellation", t.ID())
		// The cancellation request for t will go stale an hour after its deadline
		s.toCancel[t.ID()] = t.Deadline().Add(time.Hour)
	}
	// Delete the range
	if err := s.S.Delete(ctx, id.String()); err != nil {
		lg.Printf("Error deleting range: %v", err)
		return err
	}
	// Remove the job from the scheduler
	delete(s.jobs, id)
	delete(s.files, id)
	delete(s.specs, id)
	delete(s.tasks, id)
	// Delete the job directory
	if err := fs.RemoveAll(ctx, s.fs, s.jobDir(id)); err != nil {
		lg.Printf("Error removing job directory: %v", err)
		return err
	}
	// Provide feedback and return success
	lg.Printf("Deleted job %v", id.String())
	return nil
}

// handleSubmit handles Submit requests.
func (s *scheduler) handleSubmit(ctx context.Context, req *request) *response {
	lg := log.PrefixWith(s.Log(), "[handleSubmit]")
	// Make an ID
	u, err := ulid.New()
	if err != nil {
		lg.Printf("error making ID: %v", err)
		return wrap(err)
	}
	id := job.ID(u)
	// Make the job
	j, err := newJb(req.spec, id)
	if err != nil {
		return wrap(err)
	}
	// Make the job directory
	if err = fs.MkdirRecursive(ctx, s.fs, s.jobDir(id)); err != nil {
		lg.Printf("Error creating job directory: %v", err)
		return wrap(err)
	}
	// Create the range. Note that req.spec is non-nil here, because otherwise the call to newJb would have failed.
	name := id.String()
	R := req.spec.Range
	lifetime := req.spec.Lifetime
	maxRetries := req.spec.MaxRetries
	maxConcurrency := req.spec.MaxConcurrency
	if err := s.S.Create(ctx, name, R, lifetime, maxRetries, maxConcurrency); err != nil {
		lg.Printf("error creating range: %v", err)
		return wrap(err)
	}
	lg.Printf("created range %s", name)
	// Record the job and specification
	s.pending[id] = j
	s.pendingSpecs[id] = req.spec
	// Allocate the map that records file locations
	s.files[id] = make(map[string]string)
	// The job will be cancelled if we don't receive all the files within an hour
	s.cancelPending[id] = time.Now().Add(1 * time.Hour)
	// Return the job ID
	return &response{id: id}
}

// handleSubmitFile handles SubmitFile requests.
func (s *scheduler) handleSubmitFile(ctx context.Context, req *request) *response {
	lg := log.PrefixWith(s.Log(), "[handleSubmitFile]")
	// Sanity checks
	id := req.id
	lg.Printf("got id: %s", id)
	spec, ok := s.pendingSpecs[id]
	if !ok {
		return wrap(jerrors.ErrUnknownJob)
	}
	if !spec.Manifest.ContainsFile(req.manifestPath) {
		lg.Printf("unknown file: %s", req.manifestPath)
		return wrap(jerrors.ErrUnknownFile)
	}
	// Record the fs path to this file
	s.files[id][req.manifestPath] = req.path
	// Check if we have all the files
	if len(s.files[id]) == len(spec.Manifest.Files()) {
		lg.Printf("All files are uploaded. Activating job %s", id)
		// We have all the files now, so move the job and spec across
		s.jobs[id] = s.pending[id]
		delete(s.pending, id)
		s.specs[id] = s.pendingSpecs[id]
		delete(s.pendingSpecs, id)
		// Remove the job from the list of pending jobs to be cancelled
		delete(s.cancelPending, id)
		// Create the set of active tasks
		s.tasks[id] = make(taskSet)
	}
	return wrap(nil)
}

// handleStatus handles Status requests.
func (s *scheduler) handleStatus(ctx context.Context, req *request) *response {
	lg := log.PrefixWith(s.Log(), "[handleStatus]")
	// Hand off to the range server
	st, err := s.S.Status(ctx, req.id.String())
	if err == irerrors.ErrUnknownRange {
		err = jerrors.ErrUnknownJob
	}
	if err != nil {
		lg.Printf("got error: %v", err)
		return wrap(err)
	}
	return &response{status: st}
}

// handleInfo handles Info requests.
func (s *scheduler) handleInfo(ctx context.Context, req *request) *response {
	lg := log.PrefixWith(s.Log(), "[handleInfo]")
	// Hand off to the range server
	inf, err := s.S.Info(ctx, req.id.String(), req.value)
	if err == irerrors.ErrUnknownRange {
		err = jerrors.ErrUnknownJob
	} else if err == irerrors.ErrNotInRange {
		err = jerrors.ErrNotInRange
	}
	if err != nil {
		lg.Printf("got error: %v", err)
		return wrap(err)
	}
	lg.Printf("success")
	return &response{info: wrapInfo{inf}}
}

// handleList handles List requests.
func (s *scheduler) handleList(_ context.Context, req *request) *response {
	result := make([]job.ID, 0, len(s.jobs))
	for id := range s.jobs {
		result = append(result, id)
	}
	return &response{jobIDs: result}
}

// handleListWithPriority handles ListWithPriority requests.
func (s *scheduler) handleListWithPriority(_ context.Context, req *request) *response {
	result := make([]job.ID, 0, len(s.jobs))
	for id, j := range s.jobs {
		if j.Priority == req.level {
			result = append(result, id)
		}
	}
	return &response{jobIDs: result}
}

// handleDelete handles Delete requests.
func (s *scheduler) handleDelete(ctx context.Context, req *request) *response {
	err := s.deleteJob(ctx, req.id)
	return wrap(err)
}

// handleDescribe handles Describe requests.
func (s *scheduler) handleDescribe(_ context.Context, req *request) *response {
	lg := log.PrefixWith(s.Log(), "[handleDescribe]")
	spec, ok := s.specs[req.id]
	if !ok {
		lg.Printf("unknown job: %v", req.id.String())
		return wrap(jerrors.ErrUnknownJob)
	}
	return &response{spec: spec}
}

// handleRetrieveFile handles RetrieveFile requests.
func (s *scheduler) handleRetrieveFile(_ context.Context, req *request) *response {
	// Sanity checks
	id := req.id
	uploadedFiles, ok := s.files[id]
	if !ok {
		return wrap(jerrors.ErrUnknownJob)
	}
	ourPath, ok := uploadedFiles[req.manifestPath]
	if !ok {
		return wrap(jerrors.ErrUnknownFile)
	}
	return &response{path: ourPath}
}

// handleSuccess handles Success requests.
func (s *scheduler) handleSuccess(ctx context.Context, req *request) *response {
	err := s.processTask(ctx, req.task, SuccessReq)
	return wrap(err)
}

// handleError handles Error requests.
func (s *scheduler) handleError(ctx context.Context, req *request) *response {
	err := s.processTask(ctx, req.task, ErrorReq)
	return wrap(err)

}

// handleRequeue handles Requeue requests.
func (s *scheduler) handleRequeue(ctx context.Context, req *request) *response {
	err := s.processTask(ctx, req.task, RequeueReq)
	return wrap(err)
}

// handleFatal handles Fatal requests.
func (s *scheduler) handleFatal(ctx context.Context, req *request) *response {
	err := s.processTask(ctx, req.task, FatalReq)
	return wrap(err)
}

// handleHeartbeat handles Heartbeat requests.
func (s *scheduler) handleHeartbeat(_ context.Context, req *request) *response {
	lg := log.PrefixWith(s.Log(), "[handleHeartbeat]")
	// Grab the task
	t := req.task
	if t == nil {
		// a nil *Task indicates that the worker is currently idle
		return &response{carryOn: true}
	}
	// Log the heartbeat
	id := t.ID()
	lg.Printf("Received heartbeat for task %v", id)
	// Check if this task has been cancelled
	carryOn := true
	for x := range s.toCancel {
		if x == id {
			lg.Printf("Requesting cancellation for task %v", id)
			carryOn = false
		}
	}
	delete(s.toCancel, id)
	return &response{carryOn: carryOn}
}

// handleActivate handles Activate requests.
func (s *scheduler) handleActivate(_ context.Context, req *request) *response {
	lg := log.PrefixWith(s.Log(), "[handleActivate]")
	// Grab the task
	t := req.task
	if t == nil {
		lg.Printf("got nil *Task")
		return wrap(jerrors.ErrNilTask)
	}
	// Mark the task as active
	id := t.ID()
	lg.Printf("Marking task %v as active", id.String())
	s.tasks[t.JobID()][t.ID()] = t
	// Return success
	return wrap(nil)
}

// handle passes the request req to the appropriate handler function and returns its response.
func (s *scheduler) handle(req *request) *response {
	lg := log.PrefixWith(s.Log(), "[handle]")
	// Handle the nil case
	if s == nil {
		lg.Printf("Got nil request")
		return wrap(errors.New("illegal nil request"))
	}
	// Create a context for the request
	ctx, cancel := context.WithCancel(context.Background())
	if req.doneC != nil {
		// Ensure that cancel gets called when the done channel closes
		defer contextutil.NewChannelLink(req.doneC, cancel).Stop()
	}
	defer cancel()
	// Hand off
	var resp *response
	switch req.code {
	case SubmitReq:
		resp = s.handleSubmit(ctx, req)
	case SubmitFileReq:
		resp = s.handleSubmitFile(ctx, req)
	case StatusReq:
		resp = s.handleStatus(ctx, req)
	case InfoReq:
		resp = s.handleInfo(ctx, req)
	case ListReq:
		resp = s.handleList(ctx, req)
	case ListWithPriorityReq:
		resp = s.handleListWithPriority(ctx, req)
	case DeleteReq:
		resp = s.handleDelete(ctx, req)
	case DescribeReq:
		resp = s.handleDescribe(ctx, req)
	case RetrieveFileReq:
		resp = s.handleRetrieveFile(ctx, req)
	case SuccessReq:
		resp = s.handleSuccess(ctx, req)
	case ErrorReq:
		resp = s.handleError(ctx, req)
	case RequeueReq:
		resp = s.handleRequeue(ctx, req)
	case FatalReq:
		resp = s.handleFatal(ctx, req)
	case HeartbeatReq:
		resp = s.handleHeartbeat(ctx, req)
	case ActivateReq:
		resp = s.handleActivate(ctx, req)
	default:
		resp = wrap(fmt.Errorf("unknown request code: %v", req.code))
	}
	return resp
}

// scheduleWorker is the worker that maintains s. On creation of s, this is run in its own goroutine.
func (s *scheduler) scheduleWorker() {
	lg := log.PrefixWith(s.Log(), "[scheduleWorker]")
	// Try to get a job
	hasJob, j := s.nextJob()
	// The main loop
	ok := true
	for ok {
		if hasJob {
			select {
			case s.jobC <- j:
				lg.Printf("released job %v with priority %v", j.ID, j.Priority)
				// Try to get a new job
				hasJob, j = s.nextJob()
			case req := <-s.reqC:
				// We received a request, so our stored job is no longer valid
				hasJob = false
				// Respond to the request
				s.respC <- s.handle(req)
			case <-s.doneC:
				// Shut down
				ok = false
			}
		} else {
			// Make a channel down which any newly-available job will be passed
			C, shutdownC, blockC := s.fanIn()
			select {
			case j = <-C:
				hasJob = true
				// Shut down the job listener
				close(shutdownC)
				<-blockC
			case req := <-s.reqC:
				// Shut down the job listener, then respond to the request
				close(shutdownC)
				<-blockC
				s.respC <- s.handle(req)
			case <-s.doneC:
				// Shut down
				ok = false
			}
		}
		// Remove any stale tasks from the collection of tasks to be cancelled
		s.pruneStale()
		// Remove any stale pending jobs
		s.prunePending()
	}
	// Remove all temporary files
	fs.RemoveAll(context.Background(), s.fs, s.workingDir)
	// Close the channels and exit
	close(s.jobC)
	close(s.reqC)
	close(s.respC)
}
