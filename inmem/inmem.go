// Inmem defines an in-memory implementation of the pcas job scheduling system.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package inmem

import (
	"bitbucket.org/pcas/fs"
	"bitbucket.org/pcas/irange"
	ir "bitbucket.org/pcas/irange/inmem"
	"bitbucket.org/pcas/job"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/ulid"
	"context"
	"errors"
	"fmt"
	"io"
	"path"
	"sync"
	"time"
)

// taskSet represents a set of tasks. We use a map from TaskIDs to Tasks, rather than e.g. a map from Tasks to booleans, so that the keys are concrete types not interfaces.
type taskSet map[job.TaskID]job.Task

// scheduler is the in-memory job scheduler. It satisfies both the job.Client and job.Worker interfaces.
type scheduler struct {
	log.BasicLogable

	// Every scheduler has an associated scheduleWorker, which is responsible for maintaining the following channels.
	jobC  chan *jb       // a channel from which to read the next job with available tasks
	reqC  chan *request  // a channel for requests to the scheduleWorker
	respC chan *response // a channel for responses from the scheduleWorker

	// Close the following channel to shut down the scheduleWorker
	doneC chan struct{}

	S          irange.Storage // the underlying range storage system
	fs         fs.Interface   // the file storage for Manifest files
	workingDir string         // the path on fs under which all files are stored

	m        sync.RWMutex // a mutex protecting isClosed
	isClosed bool         // true if and only if this scheduler has been closed

	// The following fields should only be accessed by the scheduleWorker.
	pending       map[job.ID]*jb                // the pending jobs, that is, those that have been submitted but for which we do not yet have all files in the Manifest
	pendingSpecs  map[job.ID]*job.Specification // the specifications of pending jobs
	cancelPending map[job.ID]time.Time          // the times at which pending jobs should be cancelled
	jobs          map[job.ID]*jb                // the active jobs
	files         map[job.ID]map[string]string  // for each job, a mapping from paths (as in the job Manifest) to the path of the corresponding file on fs
	specs         map[job.ID]*job.Specification // the specifications of active jobs
	tasks         map[job.ID]taskSet            // the IDs of probably-active tasks. Note that these sets are not definitive, because tasks can have timed out on the range server S without being removed from the sets.
	toCancel      map[job.TaskID]time.Time      // the IDs of tasks to be cancelled, together with the times at which these IDs can safely be removed from toCancel
}

// Assert that the scheduler satisfies the job.Storage interface
var _ job.Storage = &scheduler{}

// Errors
var (
	ErrNilScheduler = errors.New("illegal nil *scheduler")
	ErrClosed       = errors.New("the scheduler is closed")
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// withContext populates the done channel of req with a channel that will close when ctx fires, and returns req together with a channel C. The channel C must be closed once req is finished with, otherwise resources will leak.
func withContext(ctx context.Context, req *request) (*request, chan struct{}) {
	req.doneC = make(chan struct{})
	C := make(chan struct{})
	go func() {
		// Wait for the context to fire or for C to close
		select {
		case <-ctx.Done():
		case <-C:
		}
		close(req.doneC)
	}()
	return req, C
}

/////////////////////////////////////////////////////////////////////////
// private methods
/////////////////////////////////////////////////////////////////////////

// nilOrClosed returns an appropriate error if s is nil or closed, and nil otherwise.
func (s *scheduler) nilOrClosed() error {
	// Check if s is nil
	if s == nil {
		return ErrNilScheduler
	}
	// Check if s is closed
	s.m.RLock()
	defer s.m.RUnlock()
	if s.isClosed {
		return ErrClosed
	}
	return nil
}

// jobDir returns the path to the directory on s.fs where files for the job with the given ID are stored.
func (s *scheduler) jobDir(id job.ID) string {
	// We return the path /workingDir/jobID
	return path.Join(s.workingDir, id.String())
}

/////////////////////////////////////////////////////////////////////////
// job.SubmitterStorage methods
/////////////////////////////////////////////////////////////////////////

// Submit submits a job, returning an ID which identifies that job.
func (s *scheduler) Submit(ctx context.Context, j *job.Specification) (job.ID, error) {
	// Sanity checks
	if err := s.nilOrClosed(); err != nil {
		return job.NilID, err
	} else if err = j.Validate(); err != nil {
		return job.NilID, err
	}
	lg := log.PrefixWith(s.Log(), "[Submit]")
	// Make the request
	req, doneC := withContext(ctx, &request{code: SubmitReq, spec: j})
	defer close(doneC)
	select {
	case s.reqC <- req:
		// The request was made
	case <-ctx.Done():
		// Our context told us to abort
		return job.NilID, ctx.Err()
	}
	// Read the response
	resp := <-s.respC
	// Handle any error
	if resp.err != nil {
		lg.Printf("Error submitting job: %v", resp.err)
		return job.NilID, resp.err
	}
	return resp.id, nil
}

// Upload uploads a file with the path p and contents as in r. The path should be present in the manifest for the job with the given ID.
func (s *scheduler) Upload(ctx context.Context, id job.ID, p string, r io.Reader) (err error) {
	// Sanity checks
	if err = s.nilOrClosed(); err != nil {
		return
	}
	lg := log.PrefixWith(s.Log(), "[Upload]")
	// Make a new random path on our fs storage, in the directory for the job
	var u ulid.ULID
	if u, err = ulid.New(); err != nil {
		return
	}
	ourPath := path.Join(s.jobDir(id), u.String())
	// Open the file for writing
	var w io.WriteCloser
	if w, err = s.fs.Upload(ctx, ourPath); err != nil {
		lg.Printf("Error getting writer: %v", err)
		return
	}
	defer func() {
		w.Close()
		if err != nil {
			s.fs.Remove(context.Background(), ourPath)
		}
	}()
	// Copy the contents across
	if _, err = io.Copy(w, r); err != nil {
		lg.Printf("Error copying file contents: %v", err)
		return
	}
	lg.Printf("Copied file contents")
	// Make the request
	req, doneC := withContext(ctx, &request{code: SubmitFileReq, id: id, manifestPath: p, path: ourPath})
	defer close(doneC)
	select {
	case s.reqC <- req:
		// The request was made
	case <-ctx.Done():
		// Our context told us to abort
		err = ctx.Err()
		return
	}
	// Read the response
	resp := <-s.respC
	err = resp.err
	return
}

// Status returns the status of the job with the given ID.
func (s *scheduler) Status(ctx context.Context, id job.ID) (job.Status, error) {
	// Sanity checks
	if err := s.nilOrClosed(); err != nil {
		return nil, err
	}
	// Make the request
	req, doneC := withContext(ctx, &request{code: StatusReq, id: id})
	defer close(doneC)
	select {
	case s.reqC <- req:
		// The request was made
	case <-ctx.Done():
		// Our context told us to abort
		return nil, ctx.Err()
	}
	// Read the response
	resp := <-s.respC
	// Propagate any error
	if resp.err != nil {
		return nil, resp.err
	}
	return resp.status, nil
}

// Info returns information about the task with the given value for the job with the given ID.
func (s *scheduler) Info(ctx context.Context, id job.ID, value int64) (job.Info, error) {
	// Sanity checks
	if err := s.nilOrClosed(); err != nil {
		return nil, err
	}
	// Make the request
	req, doneC := withContext(ctx, &request{code: InfoReq, id: id, value: value})
	defer close(doneC)
	select {
	case s.reqC <- req:
		// The request was made
	case <-ctx.Done():
		// Our context told us to abort
		return nil, ctx.Err()
	}
	// Read the response
	resp := <-s.respC
	// Propagate any error
	if resp.err != nil {
		return nil, resp.err
	}
	return resp.info, nil
}

// List returns a slice containing the IDs of all jobs
func (s *scheduler) List(ctx context.Context) ([]job.ID, error) { // Sanity check
	// Sanity checks
	if err := s.nilOrClosed(); err != nil {
		return nil, err
	}
	// Make the request
	req, doneC := withContext(ctx, &request{code: ListReq})
	defer close(doneC)
	select {
	case s.reqC <- req:
		// The request was made
	case <-ctx.Done():
		// Our context told us to abort
		return nil, ctx.Err()
	}
	// Read the response
	resp := <-s.respC
	// Propagate any error
	if resp.err != nil {
		return nil, resp.err
	}
	return resp.jobIDs, nil
}

// ListWithPriority returns a slice containing the IDs of all jobs with the given priority level.
func (s *scheduler) ListWithPriority(ctx context.Context, level job.Priority) ([]job.ID, error) {
	// Sanity checks
	if err := s.nilOrClosed(); err != nil {
		return nil, err
	}
	// Make the request
	req, doneC := withContext(ctx, &request{code: ListWithPriorityReq, level: level})
	defer close(doneC)
	select {
	case s.reqC <- req:
		// The request was made
	case <-ctx.Done():
		// Our context told us to abort
		return nil, ctx.Err()
	}
	// Read the response
	resp := <-s.respC
	// Propagate any error
	if resp.err != nil {
		return nil, resp.err
	}
	return resp.jobIDs, nil
}

// Delete deletes the job with given ID.
func (s *scheduler) Delete(ctx context.Context, id job.ID) error {
	// Sanity checks
	if err := s.nilOrClosed(); err != nil {
		return err
	}
	// Make the request
	req, doneC := withContext(ctx, &request{code: DeleteReq, id: id})
	defer close(doneC)
	select {
	case s.reqC <- req:
		// The request was made
	case <-ctx.Done():
		// Our context told us to abort
		return ctx.Err()
	}
	// Read the response and return
	resp := <-s.respC
	return resp.err
}

// Describe returns the submission data for the job with the given ID.
func (s *scheduler) Describe(ctx context.Context, id job.ID) (*job.Specification, error) {
	// Sanity checks
	if err := s.nilOrClosed(); err != nil {
		return nil, err
	}
	// Make the request
	req, doneC := withContext(ctx, &request{code: DescribeReq, id: id})
	defer close(doneC)
	select {
	case s.reqC <- req:
		// The request was made
	case <-ctx.Done():
		// Our context told us to abort
		return nil, ctx.Err()
	}
	// Read the response
	resp := <-s.respC
	// Propagate any error
	if resp.err != nil {
		return nil, resp.err
	}
	return resp.spec, nil
}

/////////////////////////////////////////////////////////////////////////
// job.WorkerStorage methods
/////////////////////////////////////////////////////////////////////////

// Next returns a new task, if one is available. It provides worker metadata m to the job server.
func (s *scheduler) Next(ctx context.Context, m job.Metadata) (job.Task, error) {
	// Sanity checks
	if err := s.nilOrClosed(); err != nil {
		return nil, err
	}
	lg := log.PrefixWith(s.Log(), "[Next]")
	lg.Printf("started")
	// Try to read a job from the job channel
	var j *jb
	select {
	case j = <-s.jobC:
		// continue
	case <-ctx.Done():
		// Our context told us to abort
		return nil, ctx.Err()
	}
	lg.Printf("read job %v", j.ID)
	// Get a new task
	e, err := s.S.Next(ctx, j.ID.String(), &irange.Metadata{AppName: m.AppName, Hostname: m.Hostname})
	if err != nil {
		lg.Printf("error getting task: %v", err)
		return nil, err
	}
	t := &tsk{j: j, e: e}
	lg.Printf("about to activate task %v", t.ID().String())
	// Mark the task as active
	req, doneC := withContext(ctx, &request{code: ActivateReq, task: t})
	defer close(doneC)
	select {
	case s.reqC <- req:
		// The request was made
	case <-ctx.Done():
		// Our context told us to abort
		return nil, ctx.Err()
	}
	lg.Printf("request made")
	// Read the response and return
	resp := <-s.respC
	if resp.err != nil {
		lg.Printf("error activating task: %v", err)
		return nil, resp.err
	}
	lg.Printf("success")
	return t, nil
}

// Download downloads a file with the path p. This should be present in the manifest for the job with the given ID. It is the caller's responsibility to call Close on the returned ReadCloser, otherwise resources may leak.
func (s *scheduler) Download(ctx context.Context, id job.ID, p string) (job.Reader, error) {
	// Sanity checks
	if err := s.nilOrClosed(); err != nil {
		return nil, err
	}
	// Make the request
	req, doneC := withContext(ctx, &request{code: RetrieveFileReq, id: id, manifestPath: p})
	defer close(doneC)
	select {
	case s.reqC <- req:
		// The request was made
	case <-ctx.Done():
		// Our context told us to abort
		return nil, ctx.Err()
	}
	// Read the response
	resp := <-s.respC
	if err := resp.err; err != nil {
		return nil, err
	}
	// Open the file for reading
	return s.fs.Download(ctx, resp.path)
}

// Success indicates that the task t has succeeded.
func (s *scheduler) Success(ctx context.Context, t job.Task) error {
	// Sanity checks
	if err := s.nilOrClosed(); err != nil {
		return err
	}
	// Make the request
	req, doneC := withContext(ctx, &request{code: SuccessReq, task: t})
	defer close(doneC)
	select {
	case s.reqC <- req:
		// The request was made
	case <-ctx.Done():
		// Our context told us to abort
		return ctx.Err()
	}
	// Read the response and return
	resp := <-s.respC
	return resp.err
}

// Error indicates that the task t has failed and should be retried.
func (s *scheduler) Error(ctx context.Context, t job.Task) error {
	// Sanity checks
	if err := s.nilOrClosed(); err != nil {
		return err
	}
	// Make the request
	req, doneC := withContext(ctx, &request{code: ErrorReq, task: t})
	defer close(doneC)
	select {
	case s.reqC <- req:
		// The request was made
	case <-ctx.Done():
		// Our context told us to abort
		return ctx.Err()
	}
	// Read the response and return
	resp := <-s.respC
	return resp.err
}

// Requeue indicates that the task t should be requeued, without incrementing the number of failures.
func (s *scheduler) Requeue(ctx context.Context, t job.Task) error {
	// Sanity checks
	if err := s.nilOrClosed(); err != nil {
		return err
	}
	// Make the request
	req, doneC := withContext(ctx, &request{code: RequeueReq, task: t})
	defer close(doneC)
	select {
	case s.reqC <- req:
		// The request was made
	case <-ctx.Done():
		// Our context told us to abort
		return ctx.Err()
	}
	// Read the response and return
	resp := <-s.respC
	return resp.err
}

// Fatal indicates that the task t has failed and should not be requeued.
func (s *scheduler) Fatal(ctx context.Context, t job.Task) error {
	// Sanity checks
	if err := s.nilOrClosed(); err != nil {
		return err
	}
	// Make the request
	req, doneC := withContext(ctx, &request{code: FatalReq, task: t})
	defer close(doneC)
	select {
	case s.reqC <- req:
		// The request was made
	case <-ctx.Done():
		// Our context told us to abort
		return ctx.Err()
	}
	// Read the response and return
	resp := <-s.respC
	return resp.err
}

// Heartbeat sends a message to the job server indicating that the worker is still alive and is processing the task t. A nil value for t indicates that no task is being processed. Heartbeat returns true if task processing should continue and false if the current task should be cancelled.
func (s *scheduler) Heartbeat(ctx context.Context, t job.Task) (bool, error) {
	// Sanity checks
	if err := s.nilOrClosed(); err != nil {
		return false, err
	}
	// Make the request
	req, doneC := withContext(ctx, &request{code: HeartbeatReq, task: t})
	defer close(doneC)
	select {
	case s.reqC <- req:
		// The request was made
	case <-ctx.Done():
		// Our context told us to abort
		return false, ctx.Err()
	}
	// Read the response
	resp := <-s.respC
	// Propagate any error
	if resp.err != nil {
		return false, resp.err
	}
	return resp.carryOn, nil
}

// Close closes s
func (s *scheduler) Close() error {
	if err := s.nilOrClosed(); err != nil {
		// There is nothing to do
		return nil
	}
	s.m.Lock()
	defer s.m.Unlock()
	s.isClosed = true
	close(s.doneC)
	return nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// New creates a new in-memory job server with the given configuration.
func New(ctx context.Context, cfg *Config) (job.Storage, error) {
	// Handle the nil case
	if cfg == nil {
		cfg = DefaultConfig()
	}
	// Sanity check
	if err := cfg.Validate(); err != nil {
		return nil, fmt.Errorf("invalid configuration: %w", err)
	}
	// Make the working directory
	u, err := ulid.New()
	if err != nil {
		return nil, fmt.Errorf("error whilst creating path to working directory: %w", err)
	}
	wd := "/job/inmem/" + u.String() + "/"
	if err := fs.MkdirRecursive(ctx, cfg.Fs, wd); err != nil {
		return nil, fmt.Errorf("error whilst creating working directory %s: %w", wd, err)
	}
	// Create the scheduler
	s := &scheduler{
		jobC:          make(chan *jb),
		reqC:          make(chan *request),
		respC:         make(chan *response),
		doneC:         make(chan struct{}),
		S:             ir.New(cfg.Config),
		fs:            cfg.Fs,
		workingDir:    wd,
		pending:       make(map[job.ID]*jb),
		pendingSpecs:  make(map[job.ID]*job.Specification),
		cancelPending: make(map[job.ID]time.Time),
		jobs:          make(map[job.ID]*jb),
		files:         make(map[job.ID]map[string]string),
		specs:         make(map[job.ID]*job.Specification),
		tasks:         make(map[job.ID]taskSet),
		toCancel:      make(map[job.TaskID]time.Time),
	}
	// Set the logger
	s.SetLogger(cfg.Log)
	// Launch the worker and return
	go s.scheduleWorker()
	return s, nil
}
