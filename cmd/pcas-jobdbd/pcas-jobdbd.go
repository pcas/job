// Pcas-jobdbd is a server for the pcas job storage system.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/job"
	"bitbucket.org/pcas/job/inmem"
	"bitbucket.org/pcas/job/jobdb"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/runtimemetrics"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/listenutil"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"fmt"
	"net"
	"os"
	"strconv"
	"time"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createTCPListener returns a new TCP listener.
func createTCPListener(a *address.Address, opts *Options, lg log.Interface) (net.Listener, error) {
	port := jobdb.DefaultTCPPort
	if a.HasPort() {
		port = a.Port()
	}
	return listenutil.TCPListener(a.Hostname(), port,
		listenutil.MaxNumConnections(opts.MaxNumConnections),
		listenutil.Log(lg),
	)
}

// createWebsocketListener returns a new websocket listener.
func createWebsocketListener(a *address.Address, opts *Options, lg log.Interface) (net.Listener, error) {
	port := jobdb.DefaultWSPort
	if a.HasPort() {
		port = a.Port()
	}
	uri := "ws://" + a.Hostname() + ":" + strconv.Itoa(port) + a.EscapedPath()
	l, shutdown, err := listenutil.WebsocketListenAndServe(uri,
		listenutil.MaxNumConnections(opts.MaxNumConnections),
		listenutil.Log(lg),
	)
	if err != nil {
		return nil, err
	}
	cleanup.Add(shutdown)
	return l, nil
}

// createListener returns a new listener.
func createListener(opts *Options, lg log.Interface) (l net.Listener, err error) {
	switch opts.Address.Scheme() {
	case "tcp":
		l, err = createTCPListener(opts.Address, opts, lg)
	case "ws":
		l, err = createWebsocketListener(opts.Address, opts, lg)
	default:
		err = errors.New("unsupported URI scheme: " + opts.Address.Scheme())
	}
	if err != nil {
		lg.Printf("Error starting listener: %v", err)
	}
	return
}

// reportStatus reports the status of the job with the given id on S to met.
func reportStatus(ctx context.Context, S job.SubmitterStorage, id job.ID, met metrics.Interface) {
	// Request the job details
	spec, err := S.Describe(ctx, id)
	if err != nil {
		return
	}
	// Request the status
	status, err := S.Status(ctx, id)
	if err != nil {
		return
	}
	// Build the status report
	f := make(metrics.Fields)
	f["Name"] = spec.Name
	f["ID"] = id.String()
	f["Priority"] = uint8(spec.Priority)
	f["TotalActive"] = status.Active().Len()
	f["TotalPending"] = status.Pending().Len()
	f["TotalSucceeded"] = status.Succeeded().Len()
	f["TotalFailed"] = status.Failed().Len()
	f["Active"] = status.Active().String()
	f["Pending"] = status.Pending().String()
	f["Succeeded"] = status.Succeeded().String()
	f["Failed"] = status.Failed().String()
	// Send the status report
	p, err := metrics.NewPoint(Name+".JobStatus", f, nil)
	if err != nil {
		return
	}
	met.Submit(ctx, p)
}

// reportMetrics sends a summary of the status of S to met.
func reportMetrics(ctx context.Context, S job.SubmitterStorage, met metrics.Interface) {
	// Request the ids of all current jobs
	ids, err := S.List(ctx)
	if err != nil {
		return
	}
	// Report the ids to the metrics server
	f := metrics.Fields{"NumberOfJobs": len(ids)}
	for j, id := range ids {
		f[strconv.Itoa(j)] = id.String()
	}
	p, err := metrics.NewPoint(Name+".ActiveJobs", f, nil)
	if err != nil {
		return
	}
	met.Submit(ctx, p)
	// Report the status of each job
	for _, id := range ids {
		go reportStatus(ctx, S, id, met)
	}
}

// metricsReporter provides periodic status reports to a metrics server. It reports the status of s to met every pulse, and exits when ctx fires.
func metricsReporter(ctx context.Context, s job.SubmitterStorage, met metrics.Interface, pulse time.Duration) {
	t := time.NewTimer(pulse)
	for {
		select {
		case <-ctx.Done():
			// we shut down
			if !t.Stop() {
				<-t.C
			}
			return
		case <-t.C:
			// we provide a summary report and then reset the timer
			reportMetrics(ctx, s, met)
			t.Reset(pulse)
		}
	}
}

// run starts up and runs the server. Use the given context to shut down.
func run(ctx context.Context, opts *Options, lg log.Interface, met metrics.Interface) error {
	// Create the in-memory job storage
	S, err := inmem.New(ctx, opts.Config)
	if err != nil {
		return err
	}
	S.(log.SetLoggerer).SetLogger(log.PrefixWith(lg, "[inmem]"))
	// Build the options
	serverOpts := []jobdb.Option{
		jobdb.WithStorage(S),
	}
	if len(opts.SSLKey) == 0 {
		lg.Printf("SSL is disabled: connections to this server are not encrypted")
	} else {
		serverOpts = append(serverOpts, jobdb.SSLCertAndKey(opts.SSLKeyCert, opts.SSLKey))
	}
	// Create the server
	s, err := jobdb.NewServer(serverOpts...)
	if err != nil {
		return err
	}
	s.SetLogger(log.PrefixWith(lg, "[pcas-jobdbd]"))
	s.SetMetrics(met)
	// If the context fires, stop the server
	go func() {
		<-ctx.Done()
		s.GracefulStop()
	}()
	// Launch the metrics reporter
	go metricsReporter(ctx, S, met, opts.Pulse)
	// Create the listener and serve any incoming connections
	l, err := createListener(opts, lg)
	if err != nil {
		return err
	}
	defer l.Close()
	return s.Serve(l)
}

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main function, returning any errors.
func runMain() (err error) {
	// Defer running cleanup functions
	defer func() {
		if e := cleanup.Run(); err == nil {
			err = e
		}
	}()
	// Parse the options and make a note of the logger and metrics
	opts := setOptions()
	lg := log.Log()
	mt := metrics.Metrics()
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, lg)
	// Record CPU usage etc
	defer func() {
		if e := runtimemetrics.Start(mt).Stop(); err == nil {
			err = e
		}
	}()
	// Recover and log any server panics
	defer func() {
		if e := recover(); e != nil {
			lg.Printf("Panic in main: %v", e)
			if err == nil {
				err = fmt.Errorf("panic in main: %v", e)
			}
		}
	}()
	// Run the server
	err = run(ctx, opts, lg, mt)
	return
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
