// Config.go handles configuration and logging for the pcas-fsd server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/fs"
	"bitbucket.org/pcas/fs/filesystem"
	"bitbucket.org/pcas/fs/fsd"
	"bitbucket.org/pcas/fs/fsd/fsdflag"
	"bitbucket.org/pcas/job/inmem"
	"bitbucket.org/pcas/job/jobdb"
	"bitbucket.org/pcas/logger/logd/logdflag"
	"bitbucket.org/pcas/metrics/metricsdb/metricsdbflag"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/version"
	"context"
	"errors"
	"fmt"
	"math"
	"os"
	"strings"
	"time"
)

// Options describes the options.
type Options struct {
	*inmem.Config                      // Configuration data for the in-memory job storage engine
	Address           *address.Address // The address to bind to
	MaxNumConnections int              // The maximum number of simultaneous connections allowed
	SSLKey            []byte           // The SSL private key
	SSLKeyCert        []byte           // The SSL private key certificate
	Pulse             time.Duration    // The interval between status reports to the metrics server
}

// Name is the name of the executable.
const Name = "pcas-jobdbd"

// The default values for the arguments.
const (
	DefaultHostname          = "localhost"
	DefaultPort              = jobdb.DefaultTCPPort
	DefaultMaxNumConnections = 4096
	DefaultPulse             = 1 * time.Minute
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	// Create the default addresses
	addr, err := address.NewTCP(DefaultHostname, DefaultPort)
	if err != nil {
		panic(err) // This should never happen
	}
	// Return the default options
	return &Options{
		Config:            inmem.DefaultConfig(),
		Address:           addr,
		MaxNumConnections: DefaultMaxNumConnections,
		Pulse:             DefaultPulse,
	}
}

// validate validates the options.
func validate(opts *Options) error {
	if opts.MaxNumConnections <= 0 || opts.MaxNumConnections > math.MaxInt32 {
		return fmt.Errorf("invalid maximum number of connections (%d)", opts.MaxNumConnections)
	} else if err := opts.Validate(); err != nil {
		return fmt.Errorf("error validating in-memory storage configuration: %w", err)
	} else if opts.Pulse <= 0 {
		return errors.New("illegal negative value for pulse")
	}
	return nil
}

// setStorage returns an appropriate fs.Interface to use for file storage.
func setStorage(sslClientSet *sslflag.ClientSet, withFs bool, fsdSet *fsdflag.Set, dir string) (fs.Interface, error) {
	// Sanity checks
	if !withFs && strings.TrimSpace(dir) == "" {
		return nil, fmt.Errorf("Either the -with-fs flag should be set or the -dir flag should be set to a valid directory path")
	} else if withFs && dir != "" {
		return nil, fmt.Errorf("Exactly one of the flags -with-fs and -dir should be provided")
	}
	// Handle the case where fs storage was specified explicitly
	if withFs {
		// Create the config
		c := fsdSet.ClientConfig()
		c.SSLDisabled = sslClientSet.Disabled()
		c.SSLCert = sslClientSet.Certificate()
		// Create a context with 10 second timeout
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		// Return the fs client
		return fsd.NewClient(ctx, c)
	}
	// Otherwise we were provided with a non-trivial directory
	return filesystem.New(dir)
}

// setLoggers starts the loggers as indicated by the flags.
func setLoggers(sslClientSet *sslflag.ClientSet, logdSet *logdflag.LogSet) error {
	// Note the client SSL settings
	sslDisabled := sslClientSet.Disabled()
	cert := sslClientSet.Certificate()
	// Note the loggers we'll use
	toStderr := logdSet.ToStderr()
	toServer := logdSet.ToServer()
	// Create the config
	c := logdSet.ClientConfig()
	c.SSLDisabled = sslDisabled
	c.SSLCert = cert
	// Create a context with 10 second timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Start the loggers
	return logdflag.SetLogger(ctx, toStderr, toServer, c, Name)
}

// setMetrics starts the metrics reporting.
func setMetrics(sslClientSet *sslflag.ClientSet, metricsdbSet *metricsdbflag.MetricsSet) error {
	// Note the client SSL settings
	sslDisabled := sslClientSet.Disabled()
	cert := sslClientSet.Certificate()
	// Create the config
	c := metricsdbSet.ClientConfig()
	c.SSLDisabled = sslDisabled
	c.SSLCert = cert
	// Create a context with 10 second timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Start the metrics
	return metricsdbflag.SetMetrics(ctx, c, Name)
}

// parseArgs parses the command-line flags and environment variables.
func parseArgs(opts *Options) error {
	// Define the command-line flags
	var withFs bool
	var dir string
	flag.SetGlobalHeader(fmt.Sprintf("%s is the server for the pcas job storage system.\n\nUsage: %s [options]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		address.NewFlag("address", &opts.Address, opts.Address, "The address to bind to", "The value of the flag -address should either take the form \"hostname[:port]\" or be a web-socket URI \"ws://host/path\"."),
		flag.Int("max-num-connections", &opts.MaxNumConnections, opts.MaxNumConnections, "The maximum number of connections", ""),
		flag.Int("max-retries", &opts.MaxRetries, opts.MaxRetries, "The maximum number of times that a task may be retried on error", ""),
		flag.Int("max-concurrency", &opts.MaxConcurrency, opts.MaxConcurrency, "The maximum number of simultaneously active tasks for a job", ""),
		flag.Duration("pulse", &opts.Pulse, opts.Pulse, "The interval between metrics reports", ""),
		flag.Bool("with-fs", &withFs, withFs, "Use pcas fs file storage", ""),
		flag.StringNoDefault("dir", &dir, "If set, the local directory to use for file storage", "Exactly one of the flags -with-fs and -dir should be provided."),
		&version.Flag{AppName: Name},
	)
	// Create and add the standard fsd flag set
	fsdSet := fsdflag.NewSet(nil)
	flag.AddSet(fsdSet)
	// Create and add the logd flag set
	logdSet := logdflag.NewLogSet(nil)
	flag.AddSet(logdSet)
	// Create and add the metricsdb flag set
	metricsdbSet := metricsdbflag.NewMetricsSet(nil)
	flag.AddSet(metricsdbSet)
	// Create and add the the standard SSL client set
	sslClientSet := &sslflag.ClientSet{}
	flag.AddSet(sslClientSet)
	// Create and add the standard SSL server set
	sslServerSet := &sslflag.ServerSet{}
	flag.AddSet(sslServerSet)
	// Parse the flags
	flag.Parse()
	// Recover the SSL server details
	opts.SSLKey = sslServerSet.Key()
	opts.SSLKeyCert = sslServerSet.Certificate()
	// Validate the options
	if err := validate(opts); err != nil {
		return err
	}
	// Set the backing storage
	var err error
	if opts.Fs, err = setStorage(sslClientSet, withFs, fsdSet, dir); err != nil {
		return fmt.Errorf("error creating fs storage: %w", err)
	}
	// Set the loggers
	if err := setLoggers(sslClientSet, logdSet); err != nil {
		return err
	}
	// Set the metrics
	if metricsdbSet.WithMetrics() {
		return setMetrics(sslClientSet, metricsdbSet)
	}
	return nil
}
