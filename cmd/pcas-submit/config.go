// Config.go handles configuration and argument parsing for pcas-submit.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/irange"
	"bitbucket.org/pcas/irange/irangeflag"
	"bitbucket.org/pcas/job"
	"bitbucket.org/pcas/job/jobdb"
	"bitbucket.org/pcas/job/jobdb/jobdbflag"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/convert"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/version"
	"bufio"
	"errors"
	"fmt"
	"math"
	"os"
	"path/filepath"
	"strings"
	"time"
)

// Options describes the options that the user can set.
type Options struct {
	*jobdb.ClientConfig
	Name           string        // the name of the job
	Manifest       job.Manifest  // the manifest for the job
	Script         string        // the file in the manifest that is run for each task in the job
	WorkingDir     string        // a directory in Manifest, either explicitly or implicitly (as a sub-path)
	Range          irange.Range  // the range of tasks that make up the job
	Priority       job.Priority  // the priority level of the job
	Lifetime       time.Duration // the maximum lifetime of a task
	MaxRetries     int           // the maximum number of times that a task is retried on error
	MaxConcurrency int           // the maximum number of simultaneously active tasks for a job
}

// Name is the name of the executable.
const Name = "pcas-submit"

// Default values
const (
	DefaultLifetime       = 24 * time.Hour // the default maximum lifetime for a task
	DefaultMaxRetries     = 7              // the default maximum number of times that a job is retried on error
	DefaultMaxConcurrency = math.MaxInt16  // the default maximum number of simultaneously active tasks for a job
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	opts := defaultOptions()
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nill. If the error is non-nill then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	return &Options{
		ClientConfig:   jobdb.DefaultConfig(),
		Range:          irange.Empty,
		WorkingDir:     "/",
		Priority:       job.Lowest,
		Lifetime:       DefaultLifetime,
		MaxRetries:     DefaultMaxRetries,
		MaxConcurrency: DefaultMaxConcurrency,
	}
}

// validate validates the options.
func validate(opts *Options) error {
	if err := opts.Validate(); err != nil {
		return err
	} else if strings.TrimSpace(opts.Script) == "" {
		return errors.New("empty script name")
	} else if !opts.Manifest.ContainsFile(opts.Script) {
		return errors.New("the script is not contained in the manifest")
	}
	return nil
}

// contentsFromDir returns slices fs, ds of files and directories in the directory dir.  The path dir is assumed to be in canonical form and a directory.
func contentsFromDir(dir string) (fs []string, ds []string, err error) {
	// Define a function that gets called for every path inside dir
	walker := func(path string, info os.FileInfo, err error) error {
		// Propagate any error upwards
		if err != nil {
			return err
		}
		// Record the files and directories that we see
		if info.IsDir() {
			ds = append(ds, path)
		} else {
			fs = append(fs, path)
		}
		return nil
	}
	// Do the walk
	if err = filepath.Walk(dir, walker); err != nil {
		fs = nil
		ds = nil
		err = fmt.Errorf("error whilst walking directory: %w", err)
	}
	return
}

// contentsFromFile reads each line of file, interprets it as a path, and returns slices fs, ds of the files and directories found.  The path file is assumed to be in canonical form and a file.
func contentsFromFile(file string) (fs []string, ds []string, err error) {
	// Open the file
	f, err := os.Open(file)
	if err != nil {
		return
	}
	defer f.Close()
	// Read line-by-line
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		var info os.FileInfo
		p := scanner.Text()
		info, err = os.Stat(p)
		if err != nil {
			fs = nil
			ds = nil
			err = fmt.Errorf("error whilst processing manifest file: %w", err)
			return
		}
		if info.IsDir() {
			ds = append(ds, p)
		} else {
			fs = append(fs, p)
		}
	}
	if err = scanner.Err(); err != nil {
		fs = nil
		ds = nil
		err = fmt.Errorf("error whilst processing manifest file: %w", err)
		return
	}
	return
}

// createManifest creates a manifest from the given script, directory, and manifest file
func createManifest(script string, dir string, file string) (job.Manifest, error) {
	// Prepare slices of files and directories
	files := []string{script}
	var directories []string
	// Add the contents of dir
	if dir != "" {
		dir = filepath.Clean(dir)
		fs, ds, err := contentsFromDir(dir)
		if err != nil {
			return job.Manifest{}, err
		}
		files = append(files, fs...)
		directories = append(directories, ds...)
	}
	// Add the contents of file
	if file != "" {
		file = filepath.Clean(file)
		fs, ds, err := contentsFromFile(file)
		if err != nil {
			return job.Manifest{}, err
		}
		files = append(files, fs...)
		directories = append(directories, ds...)
	}
	// Create the Manifest and return
	return job.NewManifest(files, directories)
}

// parseArgs parses the command-line flags.
func parseArgs(opts *Options) error {
	var logToStderr bool
	var manifest string
	var dir string
	var priority int
	// Define the flags and usage message
	flag.SetGlobalHeader(fmt.Sprintf("%s submits a job to a pcas jobdb server.\n\nUsage: %s [options] script", Name, Name))
	flag.SetGlobalFooter("The manifest for the job submission is made up of the script, the contents of the directory specified by the -dir flag (if provided), and the files and directories listed, one per line, in the file specified by the -manifest flag (if provided).")
	flag.SetName("Options")
	flag.Add(
		flag.Bool("log-to-stderr", &logToStderr, logToStderr, "Log to stderr during job submission", ""),
		flag.StringNoDefault("manifest", &manifest, "A file containing the manifest", ""),
		flag.StringNoDefault("dir", &dir, "A directory, the contents of which will be included in the job manifest", ""),
		flag.StringNoDefault("name", &opts.Name, "The name of the job (default: the script name)", ""),
		flag.String("working-dir", &opts.WorkingDir, opts.WorkingDir, "The working directory for a task", ""),
		irangeflag.New("range", &opts.Range, opts.Range, "The range of tasks for the job", "The value of the flag -range should take the form e.g. \"[-5..-3,2,12..13]\"."),
		flag.Int("priority", &priority, priority, "The priority level of the job", ""),
		flag.Duration("lifetime", &opts.Lifetime, opts.Lifetime, "The maximum lifetime of a task", ""),
		flag.Int("max-retries", &opts.MaxRetries, opts.MaxRetries, "The maximum number of times that a task should be retried on error", ""),
		flag.Int("max-concurrency", &opts.MaxConcurrency, opts.MaxConcurrency, "The maximum number of simultaneously active tasks for the job", ""),
		&version.Flag{AppName: Name},
	)
	// Create and add the fsd flag set
	jobdbSet := jobdbflag.NewSet(nil)
	flag.AddSet(jobdbSet)
	// Create and add the the standard SSL client set
	sslClientSet := &sslflag.ClientSet{}
	flag.AddSet(sslClientSet)
	// Parse the flags
	flag.Parse()
	// Recover the address etc.
	opts.ClientConfig = jobdbSet.ClientConfig()
	// Recover the SSL client details
	opts.SSLDisabled = sslClientSet.Disabled()
	opts.SSLCert = sslClientSet.Certificate()
	// Check that a script is specified
	args := flag.Args()
	if len(args) == 0 {
		return errors.New("no script specified")
	} else if len(args) > 1 {
		return errors.New("too many arguments")
	}
	// Replace the script with its full path if necessary
	var err error
	if opts.Script, err = filepath.Abs(args[0]); err != nil {
		return err
	}
	// Replace the directory with its full path if necessary
	if dir != "" {
		if dir, err = filepath.Abs(dir); err != nil {
			return err
		}
	}
	// Make the manifest
	if opts.Manifest, err = createManifest(opts.Script, dir, manifest); err != nil {
		return err
	}
	// Set the job priority
	x, err := convert.ToUint8(priority)
	if err != nil {
		return fmt.Errorf("error setting priority: %w", err)
	}
	opts.Priority = job.Priority(x)
	// Validate the options
	if err := validate(opts); err != nil {
		return err
	}
	// Set the logger and return
	if logToStderr {
		log.SetLogger(log.Stderr)
	}
	return nil
}

/////////////////////////////////////////////////////////////////////////
// Options functions
/////////////////////////////////////////////////////////////////////////

// Specification returns the job specification determined by opts.
func (opts *Options) Specification() *job.Specification {
	// Set the job name
	name := opts.Name
	if strings.TrimSpace(name) == "" {
		name = opts.Script
	}
	// Package everything up and return
	return &job.Specification{
		Name:           name,
		Manifest:       opts.Manifest,
		Script:         opts.Script,
		WorkingDir:     opts.WorkingDir,
		Range:          opts.Range,
		Priority:       opts.Priority,
		Lifetime:       opts.Lifetime,
		MaxRetries:     opts.MaxRetries,
		MaxConcurrency: opts.MaxConcurrency,
	}
}
