// Pcas-worker pulls in tasks from a pcas jobdbd server and executes them.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/fs/filesystem"
	"bitbucket.org/pcas/job"
	"bitbucket.org/pcas/job/exec"
	"bitbucket.org/pcas/job/jobdb"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"time"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// startWorker starts a worker for the job storage c with the specified timeout, grace period, logger, and metrics endpoint. The worker will initiate a graceful stop if the context ctx fires.
func startWorker(ctx context.Context, c job.WorkerStorage, timeout time.Duration, gracePeriod time.Duration, lg log.Interface, met metrics.Interface) error {
	// Grab the hostname
	hostname, err := os.Hostname()
	if err != nil {
		hostname = "unknown"
	}
	// Build the worker config
	cfg := &exec.Config{
		Timeout:  timeout,
		AppName:  Name,
		Hostname: hostname,
	}
	// Make some temporary storage for manifest files
	baseDir, err := ioutil.TempDir(os.TempDir(), "fs_*")
	if err != nil {
		return fmt.Errorf("error creating temporary directory: %w", err)
	}
	S, err := filesystem.New(baseDir)
	if err != nil {
		return fmt.Errorf("error creating temporary filesystem: %w", err)
	}
	defer os.RemoveAll(baseDir)
	// Make a new context for the worker. Our context firing should trigger graceful shutdown, not immediate cancellation, whereas the context for the worker triggers immediate cancellation.
	runCtx, cancel := context.WithCancel(context.Background())
	// Create the worker
	w := exec.NewWorker(c, S, cfg)
	w.SetLogger(lg)
	w.SetMetrics(met)
	// Launch the worker in its own goroutine
	errC := make(chan error)
	go func() {
		errC <- w.Run(runCtx)
		close(errC)
	}()
	// Ensure that ctx firing triggers graceful shutdown
	go func() {
		<-ctx.Done()
		w.Stop()
		// wait for the grace period, then trigger hard shutdown
		time.Sleep(gracePeriod)
		cancel()
	}()
	return <-errC
}

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main program, returning any errors.
func runMain() (err error) {
	// Parse the options
	opts := setOptions()
	defer cleanup.Run() // Ignore any errors
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, log.Log())
	// Create the jobdb client
	var c *jobdb.Client
	if c, err = jobdb.NewClient(ctx, opts.ClientConfig); err != nil {
		return
	}
	defer c.Close()
	// Recover and log any panics
	defer func() {
		if e := recover(); e != nil {
			log.Log().Printf("Panic in main: %v", e)
			if err == nil {
				err = fmt.Errorf("panic in main: %v", e)
			}
		}
	}()
	// Run a worker
	err = startWorker(ctx, c, opts.Timeout, opts.GracePeriod, log.Log(), metrics.Metrics())
	return
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
