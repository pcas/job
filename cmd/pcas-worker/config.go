// Config.go handles configuration and argument parsing for pcas-worker.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/fs/fsd"
	"bitbucket.org/pcas/fs/seaweed/seaweedflag"
	"bitbucket.org/pcas/irange/rangedb"
	"bitbucket.org/pcas/job/jobdb"
	"bitbucket.org/pcas/keyvalue/kvdb"
	"bitbucket.org/pcas/keyvalue/mongodb/mongodbflag"
	"bitbucket.org/pcas/keyvalue/postgres/postgresflag"
	"bitbucket.org/pcas/logger/logd"
	"bitbucket.org/pcas/logger/logd/logdflag"
	"bitbucket.org/pcas/logger/monitor/monitord"
	"bitbucket.org/pcas/metrics/metricsdb"
	"bitbucket.org/pcas/metrics/metricsdb/metricsdbflag"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/version"
	"context"
	"errors"
	"fmt"
	"os"
	"time"
)

// Options describes the configuration options for the worker.
type Options struct {
	*jobdb.ClientConfig
	Runtime     time.Duration // the length of time before we perform a graceful stop
	GracePeriod time.Duration // the length of time between initiating a graceful stop and hard shutdown
	Timeout     time.Duration // the maximum length of time to wait for a new task
}

// Name is the name of the executable.
const Name = "pcas-worker"

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nill. If the error is non-nill then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	return &Options{
		ClientConfig: jobdb.DefaultConfig(),
		Runtime:      24 * time.Hour,
		GracePeriod:  1 * time.Hour,
		Timeout:      1 * time.Minute,
	}
}

// validate validates the options.
func validate(opts *Options) error {
	if err := opts.Validate(); err != nil {
		return err
	} else if opts.Runtime <= 0 {
		return errors.New("runtime must be positive")
	}
	return nil
}

// addressEnvUsage returns a standard usage message for the address flag -f that reads its default value from the environment variable env.
func addressEnvUsage(f string, env string) string {
	var status string
	// read the environment variable and try to parse it
	val, ok := os.LookupEnv(env)
	if ok {
		_, err := address.New(val)
		if err == nil {
			status = fmt.Sprintf("current value \"%s\", which is valid", val)
		} else {
			status = fmt.Sprintf("current value \"%s\", which is invalid", val)
		}
	} else {
		status = "currently unset"
	}
	return fmt.Sprintf("The default value of the flag -%s can be specified using the environment variable %s (%s).", f, env, status)
}

// servicesSet creates and returns the services set.
func servicesSet(sslClientSet *sslflag.ClientSet) flag.Set {
	// Fetch the default configs
	fsdc := fsd.DefaultConfig()
	jobdbc := jobdb.DefaultConfig()
	kvdbc := kvdb.DefaultConfig()
	logdc := logd.DefaultConfig()
	metricsc := metricsdb.DefaultConfig()
	monitordc := monitord.DefaultConfig()
	rangedbc := rangedb.DefaultConfig()
	// Create and add the set
	s := flag.NewBasicSet("Service options")
	s.Add(
		address.NewFlag("fs-address", &fsdc.Address, fsdc.Address,
			"The address of the fs server",
			addressEnvUsage("fs-address", "PCAS_FS_ADDRESS"),
		),
		address.NewFlag("jobdb-address", &jobdbc.Address, jobdbc.Address,
			"The address of the jobdb server",
			addressEnvUsage("jobdb-address", "PCAS_JOBDB_ADDRESS"),
		),
		address.NewFlag("kvdb-address", &kvdbc.Address, kvdbc.Address,
			"The address of the kvdb server",
			addressEnvUsage("kvdb-address", "PCAS_KVDB_ADDRESS"),
		),
		address.NewFlag("log-address", &logdc.Address, logdc.Address,
			"The address of the log server",
			addressEnvUsage("log-address", "PCAS_LOG_ADDRESS"),
		),
		address.NewFlag("metrics-address", &metricsc.Address, metricsc.Address,
			"The address of the metrics server",
			addressEnvUsage("metrics-address", "PCAS_METRICS_ADDRESS"),
		),
		address.NewFlag("monitor-address", &monitordc.Address, monitordc.Address,
			"The address of the monitor server",
			addressEnvUsage("monitor-address", "PCAS_MONITOR_ADDRESS"),
		),
		address.NewFlag("rangedb-address", &rangedbc.Address, rangedbc.Address,
			"The address of the rangedb server",
			addressEnvUsage("rangedb-address", "PCAS_RANGEDB_ADDRESS"),
		),
	)
	s.SetUsageFooter("The value of the flags -*-address should either take the form \"hostname[:port]\" or be a web-socket URI \"ws://host/path\".")
	// Update the defaults on validate
	s.SetValidate(func() error {
		// Recover the SSL client details
		sslDisabled := sslClientSet.Disabled()
		sslCert := sslClientSet.Certificate()
		// Update the fsd defaults
		fsdc.SSLDisabled = sslDisabled
		fsdc.SSLCert = sslCert
		if err := fsdc.Validate(); err != nil {
			return err
		}
		fsd.SetDefaultConfig(fsdc)
		// Update the jobdb defaults
		jobdbc.SSLDisabled = sslDisabled
		jobdbc.SSLCert = sslCert
		if err := jobdbc.Validate(); err != nil {
			return err
		}
		jobdb.SetDefaultConfig(jobdbc)
		// Update the kvdb defaults
		kvdbc.AppName = Name
		kvdbc.SSLDisabled = sslDisabled
		kvdbc.SSLCert = sslCert
		if err := kvdbc.Validate(); err != nil {
			return err
		}
		kvdb.SetDefaultConfig(kvdbc)
		// Update the logd defaults
		logdc.SSLDisabled = sslDisabled
		logdc.SSLCert = sslCert
		if err := logdc.Validate(); err != nil {
			return err
		}
		logd.SetDefaultConfig(logdc)
		// Update the metrics defaults
		metricsc.SSLDisabled = sslDisabled
		metricsc.SSLCert = sslCert
		if err := metricsc.Validate(); err != nil {
			return err
		}
		metricsdb.SetDefaultConfig(metricsc)
		// Update the monitord defaults
		monitordc.SSLDisabled = sslDisabled
		monitordc.SSLCert = sslCert
		if err := monitordc.Validate(); err != nil {
			return err
		}
		monitord.SetDefaultConfig(monitordc)
		// Update the rangedb defaults
		rangedbc.SSLDisabled = sslDisabled
		rangedbc.SSLCert = sslCert
		if err := rangedbc.Validate(); err != nil {
			return err
		}
		rangedb.SetDefaultConfig(rangedbc)
		// Looks good
		return nil

	})
	// Return the set
	return s
}

// setLoggers starts the loggers as indicated by the flags.
func setLoggers(toStderr bool) error {
	// Create a context with 10 second timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Start the loggers
	return logdflag.SetLogger(ctx, toStderr, true, nil, Name)
}

// setMetrics starts the metrics reporting.
func setMetrics() error {
	// Create a context with 10 second timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Start the metrics
	return metricsdbflag.SetMetrics(ctx, nil, Name)
}

// parseArgs parses the command-line flags.
func parseArgs(opts *Options) error {
	var logToStderr bool
	// Define the flags and usage message
	flag.SetGlobalHeader(fmt.Sprintf("%s pulls in tasks from a pcas jobdb server and executes them.\n\nUsage: %s [options]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.Duration("runtime", &opts.Runtime, opts.Runtime, "The length of time after which we execute a graceful stop", ""),
		flag.Duration("grace-period", &opts.GracePeriod, opts.GracePeriod, "The length of time between initiating a graceful stop and hard shutdown", ""),
		flag.Duration("timeout", &opts.Timeout, opts.Timeout, "The maximum length of time to wait for a new task", ""),
		flag.Bool("log-to-stderr", &logToStderr, logToStderr, "Log to stderr", ""),
		&version.Flag{AppName: Name},
	)
	// Create the standard SSL client set
	sslClientSet := &sslflag.ClientSet{}
	// Add the services set
	flag.AddSet(servicesSet(sslClientSet))
	// Add the default mongodb flag set
	mongodbSet := mongodbflag.NewSet(nil)
	flag.AddSet(mongodbSet)
	// Add the default postgres flag set
	postgresSet := postgresflag.NewSet(nil)
	flag.AddSet(postgresSet)
	// Add the default seaweed flag set
	seaweedSet := seaweedflag.NewSet(nil)
	flag.AddSet(seaweedSet)
	// Add the standard SSL client set
	flag.AddSet(sslClientSet)
	// Parse the flags
	flag.Parse()
	// Set the address of the jobdb server to the value stored when parsing the services flag set
	opts.ClientConfig = jobdb.DefaultConfig()
	// Validate the options
	if err := validate(opts); err != nil {
		return err
	}
	// Set the remaining default service configurations
	mongodbSet.SetDefault(Name)
	postgresSet.SetDefault(Name)
	seaweedSet.SetDefault(Name)
	// Set the loggers
	if err := setLoggers(logToStderr); err != nil {
		return err
	}
	// Set the metrics
	return setMetrics()
}
