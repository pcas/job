// Pcas-cancel cancels a job submitted to a pcas jobdbd server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/job/jobdb"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/log"
	"context"
	"os"
)

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main program, returning any errors.
func runMain() error {
	// Parse the options
	opts := setOptions()
	defer cleanup.Run() // Ignore any errors
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, log.Log())
	// Create the jobdb client
	c, err := jobdb.NewClient(ctx, opts.ClientConfig)
	if err != nil {
		return err
	}
	c.SetLogger(log.Log())
	defer c.Close()
	// Cancel the job
	return c.Delete(ctx, opts.ID)
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
