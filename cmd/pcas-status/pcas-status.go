// Pcas-status displays status information for a pcas jobdbd server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/job"
	"bitbucket.org/pcas/job/jobdb"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"os"
	"sort"
	"strings"
	"text/tabwriter"
)

// nips represents the name, ID, priority, and status of a job
type nips struct {
	name     string
	id       job.ID
	priority job.Priority
	status   job.Status
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// getStatus queries c and returns status information for each active job, sorted by priority.
func getStatus(ctx context.Context, c job.SubmitterStorage) ([]nips, error) {
	// Request the job ids
	ids, err := c.List(ctx)
	if err != nil {
		return nil, err
	}
	// Request the name, priority, and status for each job
	S := make([]nips, 0, len(ids))
	for _, id := range ids {
		// Request the priority
		spec, err := c.Describe(ctx, id)
		if err != nil {
			return nil, err
		}
		// Request the status
		st, err := c.Status(ctx, id)
		if err != nil {
			return nil, err
		}
		S = append(S, nips{
			name:     spec.Name,
			id:       id,
			priority: spec.Priority,
			status:   st,
		})
	}
	// Sort by priority, breaking ties by name
	sort.Slice(S, func(i, j int) bool {
		return S[i].priority > S[j].priority || (S[i].priority == S[j].priority && S[i].name < S[j].name)
	})
	return S, nil
}

// headerLine returns the tab-formatted header line to display. It includes "Priority" if and only if withPriority is true and "ID" if and only if withID is true.
func headerLine(withPriority bool, withID bool) string {
	var b strings.Builder
	b.WriteString("Name")
	if withPriority {
		b.WriteString("\tPriority")
	}
	b.WriteString("\tSucceeded\tFailed\tActive\tPending")
	if withID {
		b.WriteString("\tID")
	}
	b.WriteString("\n")
	return b.String()
}

// statusLine returns a tab-formatted line describing s. It includes the priority if and only if withPriority is true and the job ID if and only if withID is true.
func statusLine(s nips, withPriority bool, withID bool) string {
	var b strings.Builder
	fmt.Fprintf(&b, "%s", s.name)
	if withPriority {
		fmt.Fprintf(&b, "\t%d", s.priority)
	}
	st := s.status
	fmt.Fprintf(&b, "\t%d\t%d\t%d\t%d", st.Succeeded().Len(), st.Failed().Len(), st.Active().Len(), st.Pending().Len())
	if withID {
		fmt.Fprintf(&b, "\t%s", s.id)
	}
	b.WriteString("\n")
	return b.String()
}

// displayStatus queries c and displays status information on os.Stdout. Priority is displayed if and only if withPriority is true. Job IDs are displayed if and only if withID is true.
func displayStatus(ctx context.Context, c job.SubmitterStorage, withPriority bool, withID bool) (err error) {
	// Request the status information
	var S []nips
	S, err = getStatus(ctx, c)
	if err != nil {
		return
	}
	// We build the output string using a tabwriter
	var output strings.Builder
	var totalActive, totalPending int
	w := tabwriter.NewWriter(&output, 12, 1, 2, ' ', 0)
	// Print the output string on exit
	defer func() {
		w.Flush()
		_, writeErr := os.Stdout.WriteString(output.String())
		if err == nil {
			err = writeErr
		}
	}()
	// Find the total number of active and pending tasks
	for _, s := range S {
		st := s.status
		totalActive += st.Active().Len()
		totalPending += st.Pending().Len()
	}
	// Display the total number of active and pending tasks
	fmt.Fprintf(w, "%d active tasks\n%d pending tasks\n", totalActive, totalPending)
	// Are there actually any jobs? If not, print what we have and exit.
	if len(S) == 0 {
		return
	}
	// Display per-job status information
	fmt.Fprintf(w, "\n%s", headerLine(withPriority, withID))
	for _, s := range S {
		fmt.Fprintf(w, "%s", statusLine(s, withPriority, withID))
	}
	return
}

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main program, returning any errors.
func runMain() error {
	// Parse the options
	opts := setOptions()
	defer cleanup.Run() // Ignore any errors
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, log.Log())
	// Create the jobdb client
	c, err := jobdb.NewClient(ctx, opts.ClientConfig)
	if err != nil {
		return err
	}
	c.SetLogger(log.Log())
	defer c.Close()
	// Query and display the status
	return displayStatus(ctx, c, opts.WithPriority, opts.WithID)
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
