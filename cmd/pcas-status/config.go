// Config.go handles configuration and argument parsing for pcas-status.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/job/jobdb"
	"bitbucket.org/pcas/job/jobdb/jobdbflag"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/version"
	"fmt"
	"os"
)

// Options describes the options that the user can set.
type Options struct {
	*jobdb.ClientConfig
	WithPriority bool // true if and only if we should display priority information
	WithID       bool // true if and only if we should display the job ID
}

// Name is the name of the executable.
const Name = "pcas-status"

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nill. If the error is non-nill then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	return &Options{
		ClientConfig: jobdb.DefaultConfig(),
	}
}

// validate validates the options.
func validate(opts *Options) error {
	if err := opts.Validate(); err != nil {
		return err
	}
	return nil
}

// parseArgs parses the command-line flags.
func parseArgs(opts *Options) error {
	var logToStderr bool
	// Define the flags and usage message
	flag.SetGlobalHeader(fmt.Sprintf("%s displays status information for a pcas jobdb server.\n\nUsage: %s [options]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.Bool("log-to-stderr", &logToStderr, logToStderr, "Log to stderr", ""),
		flag.Bool("with-priority", &opts.WithPriority, opts.WithPriority, "Display job priority", ""),
		flag.Bool("with-id", &opts.WithID, opts.WithID, "Display job ID", ""),
		&version.Flag{AppName: Name},
	)
	// Create and add the jobdb flag set
	jobdbSet := jobdbflag.NewSet(nil)
	flag.AddSet(jobdbSet)
	// Create and add the the standard SSL client set
	sslClientSet := &sslflag.ClientSet{}
	flag.AddSet(sslClientSet)
	// Parse the flags
	flag.Parse()
	// Recover the address etc.
	opts.ClientConfig = jobdbSet.ClientConfig()
	// Recover the SSL client details
	opts.SSLDisabled = sslClientSet.Disabled()
	opts.SSLCert = sslClientSet.Certificate()
	// Validate the options
	if err := validate(opts); err != nil {
		return err
	}
	// Set the logger and return
	if logToStderr {
		log.SetLogger(log.Stderr)
	}
	return nil
}
