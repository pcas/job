// Config.go handles configuration and argument parsing for pcas-dashboard.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/job/jobdb"
	"bitbucket.org/pcas/job/jobdb/jobdbflag"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/version"
	"errors"
	"fmt"
	"os"
)

// Options describes the options that the user can set.
type Options struct {
	*jobdb.ClientConfig
	Address *address.Address
}

// Name is the name of the executable.
const Name = "pcas-dashboard"

// Default values
const (
	DefaultHostname = "localhost"
	DefaultPort     = 8081
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nill. If the error is non-nill then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	// Create the default address
	addr, err := address.NewTCP(DefaultHostname, DefaultPort)
	if err != nil {
		panic(err) // This should never happen
	}
	return &Options{
		ClientConfig: jobdb.DefaultConfig(),
		Address:      addr,
	}
}

// validate validates the options.
func validate(opts *Options) error {
	if err := opts.Validate(); err != nil {
		return err
	} else if !opts.Address.IsTCP() {
		return errors.New("bind address must be a TCP port")
	}
	return nil
}

// parseArgs parses the command-line flags.
func parseArgs(opts *Options) error {
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf("%s serves a webpage that displays status information for a pcas jobdb server.\n\nUsage: %s [options]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		&version.Flag{AppName: Name},
		address.NewFlag("address", &opts.Address, opts.Address, "The address to bind to", "The value of the flag -address should take the form \"hostname[:port]\"."),
	)
	// Create and add the jobdb flag set
	jobdbSet := jobdbflag.NewSet(nil)
	flag.AddSet(jobdbSet)
	// Create and add the the standard SSL client set
	sslClientSet := &sslflag.ClientSet{}
	flag.AddSet(sslClientSet)
	// Parse the flags
	flag.Parse()
	// Recover the address etc.
	opts.ClientConfig = jobdbSet.ClientConfig()
	// Recover the SSL client details
	opts.SSLDisabled = sslClientSet.Disabled()
	opts.SSLCert = sslClientSet.Certificate()
	// Validate the options
	if err := validate(opts); err != nil {
		return err
	}
	return nil
}
