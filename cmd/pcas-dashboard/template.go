//go:generate ./template.py html/dashboard.html
/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                *** DO NOT DIRECTLY EDIT THIS FILE ***
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
This file is auto-generated from "html/dashboard.html".
To modify this file, edit "html/dashboard.html" and run "go generate".
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

const dashboardTemplate = `
<!DOCTYPE html>

<!-- This is based on examples from https://www.d3-graph-gallery.com by Yan Holtz -->

<!-- Notes to self: 
    
    - the various versions of D3 differ radically and are not compatible with each other, so make sure that you are reading documentation for the latest version. 
    - this [https://observablehq.com/@d3/selection-join] is pretty useful
    - so is this [https://bost.ocks.org/mike/selection/]
    - try out JavaScript dynamically in the Chrome console window first

-->

<!-- Add a title -->
<h1>Pcas-jobd status</h1>

<!-- Add some summary text -->
<p id="summary">0 active tasks; 0 pending tasks.</p>

<!-- Add an empty svg area -->
<div id="Area"></div>

<!-- Load d3.js -->
<script src="https://d3js.org/d3.v6.js"></script>

<script>
    // start with an empty array
    var data = [];
    
    // set the dimensions and margins of the graph
    var margin = {top: 10, right: 30, bottom: 20, left: 50},
        width = 800 - margin.left - margin.right,
        height = 400 + 10;

    // append the svg object to the body of the page
    var svg = d3.select("#Area")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom);
    
    // create a tooltip
    var tooltip = d3.select("#Area")
        .append("div")
        .style("opacity", 0)
        .attr("class", "tooltip")
        .style("background-color", "white")
        .style("padding", "5px");

    // mouseover makes the tooltip visible
    var mouseover = function(d) {
        tooltip
        .style("opacity", 1)
    };
    
    // mousemove updates the tooltip text 
    var mousemove = function(ev, d, i) {
        msg = ["Name: " + d["data"].Name, "Priority: " + d["data"].Priority, "Succeeded: " + d["data"].Succeeded, "Failed: " + d["data"].Failed, "Active: " + d["data"].Active, "Pending: " + d["data"].Pending].join(", ");
        tooltip
        .html(msg)
    };
    
    // mouseleave makes the tooltip disappear 
    var mouseleave = function(d) {
        tooltip
        .style("opacity", 0)
    };
    
    // make an SVG group to hold the chart
    var chart = svg.append("g")
        .classed("jobStatusChart", true)
        .attr("transform",
              "translate(" + margin.left + "," + margin.top + ")");

    // make another SVG group to hold the x-axis
    var xAxis = chart.append("g")
                    .classed("xAxis", true);

    // set the colours
    var colors = ['#4CAF50', '#FF0033', '#FFFF66', '#CFD8DC'];
    
    // data will hold the status data for jobs, in JSON format
    var data = [];

    // drawChart draws the job status chart using status information in data
    drawChart = function() {
        // update the chart height
        height = 20 * data.length + 10;
        svg.attr("height", height + margin.top + margin.bottom);

        // stack the data
        var stack = d3.stack()
            .keys(['Succeeded', 'Failed', 'Active', 'Pending']);
        var stackedSeries = stack(data);

        // find the maximum x value
        maxOfEach = stackedSeries[3].map(x => x[1]);
        dataMax = Math.max(...maxOfEach);

        // x scaling. We linearly scale [0,dataMax] onto [0,width] where M is the max of the data
        var x = d3.scaleLinear()
            .domain([0, dataMax])        
            .range([0, width]);     

        // update the translation and draw the x-axis
        xAxis.attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x).tickSizeOuter(0));

        // add the data to each data series
        var series = d3.select('.jobStatusChart')
                        .selectAll('.jobStatusSeries')
                        .data(stackedSeries, d => d)
                        .join(
                            enter => enter.append('g')
                                        .classed('jobStatusSeries', true)
                                        .style('fill', (d,i) => colors[i])
                        );

        // Create or update rectangles for each series in each job
        series.selectAll("rect")
            .data(d=>d)
            .join(
                enter => enter
                    .append("rect")
                    .attr('height', 19)
                    .on("mouseover", mouseover)
                    .on("mousemove", mousemove)
                    .on("mouseleave", mouseleave)
                    .attr('width', d => x(d[1]) - x(d[0]))
                    .attr('x', d => x(d[0]))
                    .attr('y', (d,i) => 20*i),
                update => update
                    .attr('width', d => x(d[1]) - x(d[0]))
                    .attr('x', d => x(d[0]))
                    .attr('y', (d,i) => 20*i)
            );
    }

    // updateChart reads job status data from "/data" and plots it on the chart 
    updateChart = function() {
        // read the data
        d3.json("/data")
            .then(function(d){
                data = d["Jobs"];
                document.getElementById("summary").innerHTML = d["TotalActive"] + " active tasks; " + d["TotalPending"] + " pending tasks."
                drawChart()
            }); 
    }

    updateChart();
    setInterval(updateChart, 5000);
    
</script>`
