#!/usr/bin/python3

'''
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
'''

import sys

# Output the usage message to stderr, then exits with a non-zero exit code.
def helpmessage():
    name = sys.argv[0]
    sys.stderr.write("""Usage: """ + name + """ inputFile.html
Output will be written to \"template.go\".
""")
    sys.exit(1)

# Outputs the given message to stderr, then exits with a non-zero exit code.
def fatalmessage(s):
    name = sys.argv[0]
    sys.stderr.write(name + ': ' + s + '\n')
    sys.exit(1)

# Validate the argument
args = sys.argv
if len(args) != 2:
    helpmessage()
src = args[1].strip()
if len(src) == 0 or src[0] == '-':
    helpmessage()
if len(src) <= 5 or src[-5:] != '.html':
    fatalmessage('Argument must have the suffix \".html\".')

# Open the output file
with open('template.go','w') as outf:
    # Write the header
    outf.write('//go:generate ./template.py ' + src + """
/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                *** DO NOT DIRECTLY EDIT THIS FILE ***
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
This file is auto-generated from \"""" + src + """\".
To modify this file, edit \"""" + src + """\" and run \"go generate\".
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

const dashboardTemplate = `
""")
    # Write the body, which is the contents of the input file
    with open(src) as inf:
        for line in inf:
            outf.write(line)
    # Write the footer
    outf.write('`\n')