// Pcas-dashboard serves a webpage that displays status information for a pcas jobdbd server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/job"
	"bitbucket.org/pcas/job/jobdb"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/log"
	"bytes"
	"context"
	"encoding/json"
	"html/template"
	"io"
	"net/http"
	"os"
	"sort"
)

// jobStatus represents status data about a job
type jobStatus struct {
	Name      string // the name of the job
	Priority  int    // the priority level of the job
	Succeeded int    // the number of tasks that have succeeded
	Failed    int    // the number of tasks that have failed
	Active    int    // the number of active tasks
	Pending   int    // the number of pending tasks
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// getStatus queries c and returns status information for each active job, sorted by priority.
func getStatus(ctx context.Context, c job.SubmitterStorage) ([]jobStatus, error) {
	// Request the job ids
	ids, err := c.List(ctx)
	if err != nil {
		return nil, err
	}
	// Request the name, priority, and status for each job
	S := make([]jobStatus, 0, len(ids))
	for _, id := range ids {
		// Request the priority
		spec, err := c.Describe(ctx, id)
		if err != nil {
			return nil, err
		}
		// Request the status
		st, err := c.Status(ctx, id)
		if err != nil {
			return nil, err
		}
		S = append(S, jobStatus{
			Name:      spec.Name,
			Priority:  int(spec.Priority),
			Succeeded: st.Succeeded().Len(),
			Failed:    st.Failed().Len(),
			Active:    st.Active().Len(),
			Pending:   st.Pending().Len(),
		})
	}
	// Sort by priority, breaking ties by name
	sort.Slice(S, func(i, j int) bool {
		return S[i].Priority > S[j].Priority || (S[i].Priority == S[j].Priority && S[i].Name < S[j].Name)
	})
	return S, nil
}

// errorResponse writes to w a 500 Internal Server Error response based on err.
func errorResponse(w http.ResponseWriter, err error) {
	http.Error(w, "internal server error: "+err.Error(), http.StatusInternalServerError)
}

// dashboardHandler is the handler for the URL "/".
func dashboardHandler(w http.ResponseWriter, r *http.Request) {
	t, err := template.New("dashboard").Parse(dashboardTemplate)
	if err != nil {
		errorResponse(w, err)
	} else if err = t.Execute(w, nil); err != nil {
		errorResponse(w, err)
	}
	return
}

// dataHandler is a handler function for the URL "/data" which reads data from c.
func dataHandler(w http.ResponseWriter, r *http.Request, c job.SubmitterStorage) {
	// Request the job status data
	S, err := getStatus(context.Background(), c)
	if err != nil {
		errorResponse(w, err)
		return
	}
	// Find the total number of active and pending tasks
	var totalActive, totalPending int
	for _, s := range S {
		totalActive += s.Active
		totalPending += s.Pending
	}
	// Create the status data
	x := map[string]interface{}{
		"TotalActive":  totalActive,
		"TotalPending": totalPending,
		"Jobs":         S,
	}
	// Encode this as JSON
	var b bytes.Buffer
	enc := json.NewEncoder(&b)
	if err := enc.Encode(x); err != nil {
		errorResponse(w, err)
	}
	io.Copy(w, &b)
}

// makeDataHandler returns a handler function for the URL "/data", which reads data from c.
func makeDataHandler(c job.SubmitterStorage) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		dataHandler(w, r, c)
	}
}

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main program, returning any errors.
func runMain() error {
	// Parse the options
	opts := setOptions()
	defer cleanup.Run() // Ignore any errors
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, log.Log())
	// Create the jobdb client
	c, err := jobdb.NewClient(ctx, opts.ClientConfig)
	if err != nil {
		return err
	}
	c.SetLogger(log.Log())
	defer c.Close()
	// Set the handler functions
	http.HandleFunc("/", dashboardHandler)
	http.HandleFunc("/data", makeDataHandler(c))
	// Start serving
	return http.ListenAndServe(opts.Address.HostnameAndPort(), nil)
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
