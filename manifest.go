// Manifest defines types and functions related to manifests for jobs.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package job

import (
	jerrors "bitbucket.org/pcas/job/errors"
	"errors"
	"path"
	"strings"
)

/*
The manifest -- the files and directories that are uploaded with a job -- could alternatively be represented by a slice of strings (paths). The information about which path is a file and which is a directory is present on submission, because Submit takes an fs.Interface which holds the files and directories, and you can query that interface to find out; the file/directory information is also available to anyone who holds a Task t, because you can query the fs.Interface t.Fs(). But the return value from Describe includes a Manifest and does not (and cannot) specify any fs.Interface. This suggests that a Manifest should be its own data type, and should contain both paths and file/directory information.
*/

// Manifest represents a collection of files and directories on a pcas fs server. Paths in a valid Manifest must be unique. A valid Manifest contains at least one file.
type Manifest struct {
	files       map[string]bool // The set of files
	directories map[string]bool // The set of directories
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// validatePaths returns an error unless paths is a (possibly empty) slice of rooted paths in canonical form.
func validatePaths(paths []string) error {
	if len(paths) == 0 {
		return nil
	}
	// Paths must be in canonical form, and rooted
	for _, p := range paths {
		if path.Clean(p) != p {
			return jerrors.ErrInvalidPaths
		} else if p[0] != '/' {
			return jerrors.ErrInvalidPaths
		}
	}
	return nil
}

/////////////////////////////////////////////////////////////////////////
// Manifest functions
/////////////////////////////////////////////////////////////////////////

// String returns a string representation of M.
func (M Manifest) String() string {
	pieces := make([]string, 0, len(M.files)+len(M.directories))
	for p := range M.files {
		pieces = append(pieces, p)
	}
	for p := range M.directories {
		pieces = append(pieces, p)
	}
	return "(" + strings.Join(pieces, ",") + ")"
}

// Files returns the files in M.
func (M Manifest) Files() []string {
	S := make([]string, 0, len(M.files))
	for p := range M.files {
		S = append(S, p)
	}
	return S
}

// Directories returns the directories in M.
func (M Manifest) Directories() []string {
	S := make([]string, 0, len(M.directories))
	for p := range M.directories {
		S = append(S, p)
	}
	return S
}

// ContainsFile returns true if and only if M contains the file with the given path.
func (M Manifest) ContainsFile(path string) bool {
	if M.files == nil {
		return false
	}
	_, ok := M.files[path]
	return ok
}

// NewManifest constructs a new Manifest containing the given files and directories.
func NewManifest(files []string, directories []string) (Manifest, error) {
	// Validate the paths
	if len(files) == 0 {
		return Manifest{}, jerrors.ErrEmptyManifest
	} else if err := validatePaths(files); err != nil {
		return Manifest{}, err
	} else if err := validatePaths(directories); err != nil {
		return Manifest{}, err
	}
	// Prepare the Manifest
	M := Manifest{
		files:       make(map[string]bool, len(files)),
		directories: make(map[string]bool, len(directories)),
	}
	// Add the files
	for _, p := range files {
		M.files[p] = true
	}
	// Add the directories, checking for overlap with files
	for _, p := range directories {
		if _, ok := M.files[p]; ok {
			return Manifest{}, errors.New("paths in a Manifest must be unique")
		}
		M.directories[p] = true
	}
	// Check uniqueness within files and directories
	if len(files) != len(M.files) || len(directories) != len(M.directories) {
		return Manifest{}, errors.New("paths in a Manifest must be unique")
	}
	// Return the Manifest
	return M, nil
}

// IsEquivalentTo returns true if and only if the paths and hashes in M and N agree after reordering.
func (M Manifest) IsEquivalentTo(N Manifest) bool {
	// Cheap check: the lengths must be the same
	if len(M.files) != len(N.files) || len(M.directories) != len(N.directories) {
		return false
	}
	// Check the files
	for p := range M.files {
		if _, ok := N.files[p]; !ok {
			return false
		}
	}
	// Check the directories
	for p := range M.directories {
		if _, ok := N.directories[p]; !ok {
			return false
		}
	}
	return true
}
