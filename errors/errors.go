// Errors defines common errors for the pcas job scheduling system.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package errors

import (
	"strconv"
)

// Code represents a common job error.
type Code uint8

// The valid error codes
const (
	ErrNilSpecification = Code(iota)
	ErrInvalidSpecification
	ErrInvalidPaths
	ErrEmptyManifest
	ErrUnknownFile
	ErrNilTask
	ErrUnknownJob
	ErrNilConfiguration
	ErrNilAddress
	ErrNotInRange
)

// codeToString maps error codes to strings.
var codeToString = map[Code]string{
	ErrNilSpecification:     "illegal nil specification",
	ErrInvalidSpecification: "invalid specification",
	ErrInvalidPaths:         "one or more paths is invalid",
	ErrEmptyManifest:        "the manifest is not allowed to be empty",
	ErrUnknownFile:          "that file is not contained in the manifest",
	ErrNilTask:              "illegal nil *Task",
	ErrUnknownJob:           "unknown job",
	ErrNilConfiguration:     "illegal nil configuration",
	ErrNilAddress:           "illegal nil address",
	ErrNotInRange:           "integer not in range",
}

// Error is the interface satisfied by an error with a Code.
type Error interface {
	// Code returns the code associated with this error.
	Code() Code
	// Error returns a string description of the error.
	Error() string
}

/////////////////////////////////////////////////////////////////////////
// Code functions
/////////////////////////////////////////////////////////////////////////

// String returns a description of the error with error code c.
func (c Code) String() string {
	s, ok := codeToString[c]
	if !ok {
		s = "Unknown error (error code: " + strconv.Itoa(int(c)) + ")"
	}
	return s
}

// Error returns a description of the error with error code c.
func (c Code) Error() string {
	return c.String()
}

// Code returns the error code c.
func (c Code) Code() Code {
	return c
}
