// Interface defines interfaces for the pcas job scheduling system.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package job

import ()

// Storage is the interface satisfied by job storage systems
type Storage interface {
	SubmitterStorage
	WorkerStorage
}
