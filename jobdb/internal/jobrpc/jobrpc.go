//go:generate protoc -I=../proto/ --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative --go_out=. --go-grpc_out=. job.proto

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package jobrpc

import (
	"encoding/json"
	"errors"

	"bitbucket.org/pcas/irange"
	"bitbucket.org/pcas/job"
	"bitbucket.org/pcastools/convert"
	"bitbucket.org/pcastools/ulid"
	"google.golang.org/protobuf/types/known/durationpb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// FromFilePath converts a string to a *FilePath.
func FromFilePath(p string) *FilePath {
	return &FilePath{Path: p}
}

// ToFilePath converts a *FilePath to a string.
func ToFilePath(fp *FilePath) (string, error) {
	if fp == nil {
		return "", errors.New("nil *FilePath in ToFilePath")
	}
	return fp.GetPath(), nil
}

// FromDirPath converts a string to a *DirPath.
func FromDirPath(p string) *DirPath {
	return &DirPath{Path: p}
}

// ToDirPath converts a *DirPath to a string.
func ToDirPath(dp *DirPath) (string, error) {
	if dp == nil {
		return "", errors.New("nil *DirPath in ToDirPath")
	}
	return dp.GetPath(), nil
}

// FromManifest converts a job.Manifest to a *Manifest.
func FromManifest(M job.Manifest) *Manifest {
	// Grab the file and directory paths
	files := M.Files()
	directories := M.Directories()
	// Convert the file and directory paths
	S := make([]*FileOrDirectory, 0, len(files)+len(directories))
	for _, p := range files {
		S = append(S, &FileOrDirectory{Path: &FileOrDirectory_File{File: FromFilePath(p)}})
	}
	for _, p := range directories {
		S = append(S, &FileOrDirectory{Path: &FileOrDirectory_Dir{Dir: FromDirPath(p)}})
	}
	return &Manifest{Paths: S}
}

// ToManifest converts a *Manifest to a job.Manifest.
func ToManifest(M *Manifest) (job.Manifest, error) {
	if M == nil {
		return job.Manifest{}, errors.New("nil *Manifest in ToManifest")
	}
	// Grab the paths
	S := M.GetPaths()
	files := make([]string, 0, len(S))
	directories := make([]string, 0, len(S))
	// Convert the paths
	for _, fd := range S {
		switch x := fd.Path.(type) {
		case *FileOrDirectory_File:
			p, err := ToFilePath(x.File)
			if err != nil {
				return job.Manifest{}, err
			}
			files = append(files, p)
		case *FileOrDirectory_Dir:
			p, err := ToDirPath(x.Dir)
			if err != nil {
				return job.Manifest{}, err
			}
			directories = append(directories, p)
		default:
			return job.Manifest{}, errors.New("unexpected type in ToManifest")
		}
	}
	// Package up the results
	return job.NewManifest(files, directories)
}

// FromMetadata converts m to a *Metadata
func FromMetadata(m map[string]string) (*Metadata, error) {
	b, err := json.Marshal(m)
	if err != nil {
		return &Metadata{}, err
	}
	return &Metadata{Json: string(b)}, nil
}

// ToMetadata converts a *Metadata to a map[string]string
func ToMetadata(mt *Metadata) (map[string]string, error) {
	if mt == nil {
		return nil, errors.New("nil *Metadata in ToMetadata")
	}
	var x map[string]string
	err := json.Unmarshal([]byte(mt.Json), &x)
	if err != nil {
		return nil, err
	}
	return x, nil
}

// FromRange converts an irange.Range to a *Range.
func FromRange(R irange.Range) *Range {
	return &Range{Range: R.String()}
}

// ToRange converts a *Range to an irange.Range.
func ToRange(R *Range) (irange.Range, error) {
	if R == nil {
		return irange.Empty, errors.New("nil *Range in ToRange")
	}
	return irange.Parse(R.Range)
}

// FromPriority converts a job.Priority to a *Priority.
func FromPriority(p job.Priority) *Priority {
	return &Priority{Priority: uint32(p)}
}

// ToPriority converts a *Priority to a job.Priority.
func ToPriority(pr *Priority) (job.Priority, error) {
	if pr == nil {
		return 0, errors.New("nil *Priority in ToPriority")
	}
	n, err := convert.ToUint8(pr.Priority)
	return job.Priority(n), err
}

// FromSpecification converts a *job.Specification to a *Specification.
func FromSpecification(spec *job.Specification) (*Specification, error) {
	if spec == nil {
		return &Specification{}, errors.New("nil *job.Specification in FromSpecification")
	}
	// Convert the metadata
	mt, err := FromMetadata(spec.Metadata)
	if err != nil {
		return &Specification{}, err
	}
	// Package everything up and return
	return &Specification{
		Name:           spec.Name,
		Manifest:       FromManifest(spec.Manifest),
		Script:         spec.Script,
		WorkingDir:     spec.WorkingDir,
		Metadata:       mt,
		Range:          FromRange(spec.Range),
		Priority:       FromPriority(spec.Priority),
		Lifetime:       durationpb.New(spec.Lifetime),
		MaxRetries:     int64(spec.MaxRetries),
		MaxConcurrency: int64(spec.MaxConcurrency),
	}, nil
}

// ToSpecification converts a *Specification into a *job.Specification.
func ToSpecification(spec *Specification) (*job.Specification, error) {
	if spec == nil {
		return nil, errors.New("nil *Specification in ToSpecification")
	}
	// Convert the manifest
	manifest, err := ToManifest(spec.GetManifest())
	if err != nil {
		return nil, err
	}
	// Convert the range
	R, err := ToRange(spec.GetRange())
	if err != nil {
		return nil, err
	}
	// Convert the priority
	priority, err := ToPriority(spec.GetPriority())
	if err != nil {
		return nil, err
	}
	// Package everything up and return
	return &job.Specification{
		Name:           spec.GetName(),
		Manifest:       manifest,
		Script:         spec.GetScript(),
		WorkingDir:     spec.GetWorkingDir(),
		Range:          R,
		Priority:       priority,
		Lifetime:       spec.GetLifetime().AsDuration(),
		MaxRetries:     int(spec.GetMaxRetries()),
		MaxConcurrency: int(spec.GetMaxConcurrency()),
	}, nil
}

// FromID converts a job.ID into a *ID
func FromID(j job.ID) *ID {
	return &ID{Id: j.String()}
}

// ToID converts a *ID into a job.ID
func ToID(id *ID) (job.ID, error) {
	if id == nil {
		return job.NilID, errors.New("nil *ID in ToID")
	}
	u, err := ulid.FromString(id.GetId())
	if err != nil {
		return job.NilID, err
	}
	return job.ID(u), nil
}

// FromTaskID converts a job.TaskID into a *TaskID.
func FromTaskID(t job.TaskID) *TaskID {
	return &TaskID{Id: t.String()}
}

// ToTaskID converts a *TaskID into a job.TaskID
func ToTaskID(id *TaskID) (job.TaskID, error) {
	if id == nil {
		return job.NilTaskID, errors.New("nil *TaskID in ToTaskID")
	}
	u, err := ulid.FromString(id.GetId())
	if err != nil {
		return job.NilTaskID, err
	}
	return job.TaskID(u), nil
}

// FromStatus converts a job.Status into a *StatusResponse.
func FromStatus(st job.Status) (*StatusResponse, error) {
	if st == nil {
		return &StatusResponse{}, errors.New("nil Status in FromStatus")
	}
	return &StatusResponse{
		Pending:   FromRange(st.Pending()),
		Active:    FromRange(st.Active()),
		Succeeded: FromRange(st.Succeeded()),
		Failed:    FromRange(st.Failed()),
	}, nil
}

// ToStatus converts a *StatusResponse into a job.Status.
func ToStatus(sr *StatusResponse) (job.Status, error) {
	if sr == nil {
		return nil, errors.New("nil *StatusResponse in ToStatus")
	}
	// Convert the ranges
	pending, err := ToRange(sr.GetPending())
	if err != nil {
		return nil, err
	}
	active, err := ToRange(sr.GetActive())
	if err != nil {
		return nil, err
	}
	succeeded, err := ToRange(sr.GetSucceeded())
	if err != nil {
		return nil, err
	}
	failed, err := ToRange(sr.GetFailed())
	if err != nil {
		return nil, err
	}
	return jobStatus{
		pending:   pending,
		active:    active,
		succeeded: succeeded,
		failed:    failed,
	}, nil
}

// FromIDAndValue converts a job.ID and int64 to a *IDAndValue.
func FromIDAndValue(id job.ID, n int64) *IDAndValue {
	return &IDAndValue{Id: FromID(id), Value: n}
}

// ToIDAndValue converts a *IDAndValue to a job.ID and an int64.
func ToIDAndValue(iv *IDAndValue) (job.ID, int64, error) {
	if iv == nil {
		return job.NilID, 0, errors.New("nil *IDAndValue in ToIDAndValue")
	}
	id, err := ToID(iv.GetId())
	if err != nil {
		return job.NilID, 0, err
	}
	return id, iv.GetValue(), nil
}

// FromState converts a job.State into a *State.
func FromState(st job.State) *State {
	return &State{State: uint32(st)}
}

// ToState converts a *State into a job.State.
func ToState(st *State) (job.State, error) {
	if st == nil {
		return 0, errors.New("nil *State in ToState")
	}
	n, err := convert.ToUint8(st.GetState())
	if err != nil {
		return 0, err
	}
	return job.State(n), nil
}

// FromInfo converts a job.Info to a *InfoResponse.
func FromInfo(inf job.Info) (*InfoResponse, error) {
	if inf == nil {
		return &InfoResponse{}, errors.New("nil job.Info in FromInfo")
	}
	// Package everything up and return
	return &InfoResponse{
		Value:    inf.Value(),
		State:    FromState(inf.State()),
		AppName:  inf.AppName(),
		Hostname: inf.Hostname(),
		Start:    timestamppb.New(inf.Start()),
		Deadline: timestamppb.New(inf.Deadline()),
		Failures: int64(inf.Failures()),
	}, nil
}

// ToInfo converts a *InfoResponse into a job.Info.
func ToInfo(ir *InfoResponse) (job.Info, error) {
	if ir == nil {
		return nil, errors.New("nil *InfoResponse in ToInfo")
	}
	// Convert the state
	state, err := ToState(ir.GetState())
	if err != nil {
		return nil, err
	}
	// Package everything up and return
	return &info{
		value:    ir.GetValue(),
		state:    state,
		appName:  ir.GetAppName(),
		hostname: ir.GetHostname(),
		start:    ir.GetStart().AsTime(),
		deadline: ir.GetDeadline().AsTime(),
		failures: int(ir.GetFailures()),
	}, nil
}

// FromIDs converts a []job.ID into a *ListResponse.
func FromIDs(ids []job.ID) (*ListResponse, error) {
	if ids == nil {
		return &ListResponse{}, errors.New("nil slice in FromIDs")
	}
	// Convert the IDs
	S := make([]*ID, 0, len(ids))
	for _, id := range ids {
		S = append(S, FromID(id))
	}
	return &ListResponse{Ids: S}, nil
}

// ToIDs converts a *ListResponse into a []job.ID.
func ToIDs(lr *ListResponse) ([]job.ID, error) {
	if lr == nil {
		return nil, errors.New("nil *ListResponse in ToIDs")
	}
	// Convert the IDs
	result := make([]job.ID, 0, len(lr.Ids))
	for _, x := range lr.Ids {
		id, err := ToID(x)
		if err != nil {
			return nil, err
		}
		result = append(result, id)
	}
	return result, nil
}

// FromTask converts a job.Task into a *Task.
func FromTask(t job.Task) (*Task, error) {
	if t == nil {
		return &Task{}, errors.New("nil job.Task in FromTask")
	}
	// Convert the metadata
	mt, err := FromMetadata(t.Metadata())
	if err != nil {
		return &Task{}, err
	}
	// Package everything up and return
	return &Task{
		Name:       t.Name(),
		Manifest:   FromManifest(t.Manifest()),
		Script:     t.Script(),
		WorkingDir: t.WorkingDir(),
		Metadata:   mt,
		Priority:   FromPriority(t.Priority()),
		JobId:      FromID(t.JobID()),
		Id:         FromTaskID(t.ID()),
		Value:      t.Value(),
		Deadline:   timestamppb.New(t.Deadline()),
		Failures:   int64(t.Failures()),
	}, nil
}

// ToTask converts a *Task into a job.Task.
func ToTask(t *Task) (job.Task, error) {
	if t == nil {
		return nil, errors.New("nil *Task in ToTask")
	}
	// Convert the task ID
	id, err := ToTaskID(t.GetId())
	if err != nil {
		return nil, err
	}
	// Handle the NilTaskID case. This represents the nil job.Task.
	if id == job.NilTaskID {
		return nil, nil
	}
	// Convert the job ID
	jobID, err := ToID(t.GetJobId())
	if err != nil {
		return nil, err
	}
	// Convert the manifest
	manifest, err := ToManifest(t.GetManifest())
	if err != nil {
		return nil, err
	}
	// Convert the metadata
	mt, err := ToMetadata(t.GetMetadata())
	if err != nil {
		return nil, err
	}
	// Convert the priority
	priority, err := ToPriority(t.GetPriority())
	if err != nil {
		return nil, err
	}
	// Package everything up and return
	return &task{
		name:       t.GetName(),
		jobID:      jobID,
		manifest:   manifest,
		workingDir: t.GetWorkingDir(),
		script:     t.GetScript(),
		metadata:   mt,
		priority:   priority,
		id:         id,
		value:      t.GetValue(),
		deadline:   t.GetDeadline().AsTime(),
		failures:   int(t.GetFailures()),
	}, nil
}

// FromWorkerMetadata converts a job.Metadata to a *WorkerMetadata.
func FromWorkerMetadata(mt job.Metadata) (*WorkerMetadata, error) {
	return &WorkerMetadata{
		AppName:  mt.AppName,
		Hostname: mt.Hostname,
	}, nil
}

// ToWorkerMetadata converts a *WorkerMetadata to a *irange.Metadata.
func ToWorkerMetadata(wm *WorkerMetadata) (job.Metadata, error) {
	if wm == nil {
		return job.Metadata{}, errors.New("nil *WorkerMetadata in ToWorkerMetadata")
	}
	return job.Metadata{
		AppName:  wm.GetAppName(),
		Hostname: wm.GetHostname(),
	}, nil
}

// FromBoolean converts a boolean into a *HeartbeatResponse.
func FromBoolean(b bool) *HeartbeatResponse {
	return &HeartbeatResponse{CarryOn: b}
}

// ToBoolean converts a *HeartbeatResponse into a boolean.
func ToBoolean(hr *HeartbeatResponse) (bool, error) {
	if hr == nil {
		return false, errors.New("nil *HeartbeatResponse in ToBoolean")
	}
	return hr.GetCarryOn(), nil
}

// FromFilePathAndID converts a string and a job.ID into a *FilePathAndID.
func FromFilePathAndID(p string, id job.ID) *FilePathAndID {
	return &FilePathAndID{
		Path: FromFilePath(p),
		Id:   FromID(id),
	}
}

// ToFilePathAndID converts a *FilePathAndID into a string and a job.ID
func ToFilePathAndID(fi *FilePathAndID) (string, job.ID, error) {
	if fi == nil {
		return "", job.NilID, errors.New("nil *FilePathAndID in ToFilePathAndID")
	}
	p, err := ToFilePath(fi.GetPath())
	if err != nil {
		return "", job.NilID, err
	}
	id, err := ToID(fi.GetId())
	if err != nil {
		return "", job.NilID, err
	}
	return p, id, nil
}

// FileChunkFromPathAndID converts the given path and id to a *FileChunk.
func FileChunkFromPathAndID(path string, id job.ID) *FileChunk {
	return &FileChunk{
		Chunk: &FileChunk_PathId{PathId: FromFilePathAndID(path, id)},
	}
}

// FileChunkFromData converts the slice b to a *FileChunk.
func FileChunkFromData(b []byte) *FileChunk {
	return &FileChunk{
		Chunk: &FileChunk_Data{Data: b},
	}
}
