// Types defines types that satisfy the interfaces in the job API.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package jobrpc

import (
	"bitbucket.org/pcas/irange"
	"bitbucket.org/pcas/job"
	"time"
)

// task represents a task.
type task struct {
	// the name of the job to which this task belongs
	name string
	// the ID of the job to which this task belongs
	jobID job.ID
	// the manifest submitted with this job
	manifest job.Manifest
	// the working directory for the job
	workingDir string
	// the script for the job
	script string
	// user-provided metadata for the job
	metadata map[string]string
	// the priority of the job
	priority job.Priority
	// the ID for this task
	id job.TaskID
	// the value for this task
	value int64
	// the deadline for this task
	deadline time.Time
	// the number of times this task has failed
	failures int
}

// task implements job.Task
var _ job.Task = &task{}

// info describes the state of a task
type info struct {
	value    int64     // the value of the task
	state    job.State // the state of the task
	appName  string    // the application name provided by the client to which this task was assigned
	hostname string    // the hostname provided by the client to which this task was assigned
	start    time.Time // the time at which this task was assigned to a client
	deadline time.Time // the time at which this task will go stale
	failures int       // the number of times that this task has failed
}

// info implements job.Info
var _ job.Info = &info{}

// jobStatus describes the status of a job
type jobStatus struct {
	pending   irange.Range
	active    irange.Range
	succeeded irange.Range
	failed    irange.Range
}

// jobStatus implements job.Status
var _ job.Status = jobStatus{}

/////////////////////////////////////////////////////////////////////////
// Task functions
/////////////////////////////////////////////////////////////////////////

// Name is the name of the job to which this task belongs
func (t *task) Name() string {
	if t == nil {
		return ""
	}
	return t.name
}

// JobID is the ID of the job
func (t *task) JobID() job.ID {
	if t == nil {
		return job.NilID
	}
	return t.jobID
}

// Manifest records the files and directories created on job submission
func (t *task) Manifest() job.Manifest {
	if t == nil {
		return job.Manifest{}
	}
	return t.manifest
}

// Script is the file in Manifest that is run for each task in the job
func (t *task) Script() string {
	if t == nil {
		return ""
	}
	return t.script
}

// WorkingDir is a directory in Manifest, either explicitly or implicitly (as a subpath)
func (t *task) WorkingDir() string {
	if t == nil {
		return ""
	}
	return t.workingDir
}

// Metadata records user-provided metadata
func (t *task) Metadata() map[string]string {
	if t == nil {
		return nil
	}
	return t.metadata
}

// Priority is the priority level of the job
func (t *task) Priority() job.Priority {
	if t == nil {
		return 0
	}
	return t.priority
}

// ID is the ID of the task
func (t *task) ID() job.TaskID {
	if t == nil {
		return job.NilTaskID
	}
	return t.id
}

// Value is the value of the task
func (t *task) Value() int64 {
	if t == nil {
		return 0
	}
	return t.value
}

// Deadline is the time at which the task will go stale
func (t *task) Deadline() time.Time {
	if t == nil {
		return time.Time{}
	}
	return t.deadline
}

// Failures is the number of times that this task has failed
func (t *task) Failures() int {
	if t == nil {
		return 0
	}
	return t.failures
}

/////////////////////////////////////////////////////////////////////////
// Info functions
/////////////////////////////////////////////////////////////////////////

// Value is the value for the task.
func (inf *info) Value() int64 {
	if inf == nil {
		return 0
	}
	return inf.value
}

// State is the state of the task.
func (inf *info) State() job.State {
	if inf == nil {
		return job.Uninitialised
	}
	return inf.state
}

// AppName is the application name provided by the client to which the task was assigned, or the empty string if there is no such client.
func (inf *info) AppName() string {
	if inf == nil {
		return ""
	}
	return inf.appName
}

// Hostname is the hostname provided by the client to which the task was assigned, or the empty string if there is no such client.
func (inf *info) Hostname() string {
	if inf == nil {
		return ""
	}
	return inf.hostname
}

// Start is the time the task was assigned to a client, or the zero time if there is no such client.
func (inf *info) Start() time.Time {
	if inf == nil {
		return time.Time{}
	}
	return inf.start
}

// Deadline is the time at which this task will go stale, or the zero time if the task has not been assigned to a client.
func (inf *info) Deadline() time.Time {
	if inf == nil {
		return time.Time{}
	}
	return inf.deadline
}

// Failures is the number of times that this task has failed.
func (inf *info) Failures() int {
	if inf == nil {
		return 0
	}
	return inf.failures
}

/////////////////////////////////////////////////////////////////////////
// Status functions
/////////////////////////////////////////////////////////////////////////

// Pending returns the range of pending tasks for the job.
func (j jobStatus) Pending() irange.Range {
	return j.pending
}

// Active returns the range of active tasks for the job.
func (j jobStatus) Active() irange.Range {
	return j.active
}

// Succeeded returns the range of tasks for the job that have succeeded.
func (j jobStatus) Succeeded() irange.Range {
	return j.succeeded
}

// Failed returns the range of tasks for the job that have failed.
func (j jobStatus) Failed() irange.Range {
	return j.failed
}
