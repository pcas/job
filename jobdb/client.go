// Client describes the client view of the jobdb server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package jobdb

import (
	"bitbucket.org/pcas/job"
	jerrors "bitbucket.org/pcas/job/errors"
	"bitbucket.org/pcas/job/jobdb/internal/jobrpc"
	"bitbucket.org/pcastools/contextutil"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/grpcutil/grpcdialer"
	"bitbucket.org/pcastools/grpcutil/grpcsnappy"
	"bitbucket.org/pcastools/log"
	"bytes"
	"context"
	"errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/protobuf/types/known/emptypb"
	"io"
	"net/url"
	"strconv"
	"sync"
)

// Client is the client view of the server.
type Client struct {
	log.BasicLogable
	io.Closer
	client   jobrpc.JobdClient // The gRPC client
	m        sync.RWMutex      // mutex protecting isClosed and closeErr
	isClosed bool              // true if and only if the worker is closed
	closeErr error             // the error, if any, on close
}

// errors
var (
	ErrNilClient = errors.New("uninitialised client")
	ErrClosed    = errors.New("client is closed")
)

// defaultChunkSize is the number of bytes sent per file chunk
const defaultChunkSize = 8192

// Assert that the client satisfies the job.Storage interface
var _ job.Storage = &Client{}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createDialFunc returns the dial function for the client or worker. Assumes that the URI has scheme "tcp" or "ws", and that a port number is set.
func createDialFunc(uri string, lg log.Interface) grpc.DialOption {
	// Parse the URI
	u, err := url.ParseRequestURI(uri)
	if err != nil {
		panic("invalid URI: " + uri)
	}
	switch u.Scheme {
	case "tcp":
		// Connect via a TCP socket
		port, err := strconv.Atoi(u.Port())
		if err != nil {
			panic("invalid URI: " + uri)
		}
		return grpcdialer.TCPDialer(u.Hostname(), port, lg)
	case "ws":
		// Connect via a websocket
		return grpcdialer.WebSocketDialer(uri, lg)
	default:
		// Unknown scheme
		panic("unsupported URI scheme: " + u.Scheme)
	}
}

/////////////////////////////////////////////////////////////////////////
// Client functions
/////////////////////////////////////////////////////////////////////////

// NewClient returns a new client view of the server specified by the configuration cfg.
func NewClient(ctx context.Context, cfg *ClientConfig) (*Client, error) {
	// Sanity checks
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	// Create a new client
	c := &Client{}
	// Set the dial options
	dialOptions := []grpc.DialOption{
		createDialFunc(cfg.URI(), c.Log()),
		grpc.WithDefaultCallOptions(
			grpc.UseCompressor(grpcsnappy.Name),
		),
		grpc.WithChainUnaryInterceptor(
			unaryClientErrorInterceptor,
		),
	}
	// Add the transport credentials
	var creds credentials.TransportCredentials
	if cfg.SSLDisabled {
		creds = insecure.NewCredentials()
	} else {
		creds = grpcutil.NewClientTLS(cfg.SSLCert)
	}
	dialOptions = append(dialOptions, grpc.WithTransportCredentials(creds))
	// Build a connection to the gRPC server
	conn, err := grpc.DialContext(ctx, cfg.Address.Hostname(), dialOptions...)
	if err != nil {
		return nil, err
	}
	// Save the connection on the client and return
	c.Closer = conn
	c.client = jobrpc.NewJobdClient(conn)
	return c, nil
}

// Close closes the client.
func (c *Client) Close() error {
	if c == nil {
		return nil
	}
	c.m.Lock()
	defer c.m.Unlock()
	if c.isClosed {
		return c.closeErr
	}
	// Close the server connection and record any error
	if err := c.Closer.Close(); err != nil {
		c.closeErr = err
	}
	c.isClosed = true
	return c.closeErr
}

// isNilOrClosed returns an appropriate error if c is nil or closed
func (c *Client) isNilOrClosed() error {
	if c == nil {
		return ErrNilClient
	}
	c.m.RLock()
	defer c.m.RUnlock()
	if c.isClosed {
		return ErrClosed
	}
	return nil
}

// Submit submits a job specified by spec, returning an ID which identifies that job.
func (c *Client) Submit(ctx context.Context, spec *job.Specification) (job.ID, error) {
	// Sanity checks
	if err := c.isNilOrClosed(); err != nil {
		return job.NilID, err
	} else if err := spec.Validate(); err != nil {
		return job.NilID, err
	}
	// Convert the specification
	s, err := jobrpc.FromSpecification(spec)
	if err != nil {
		return job.NilID, err
	}
	// Hand off to the server
	id, err := c.client.Submit(ctx, s)
	if err != nil {
		return job.NilID, err
	}
	// Convert the return value
	return jobrpc.ToID(id)
}

// Upload uploads a file with the given path and contents. The path should be present in the manifest for the job with the given ID.
func (c *Client) Upload(ctx context.Context, id job.ID, path string, contents io.Reader) error {
	// Sanity check
	if err := c.isNilOrClosed(); err != nil {
		return err
	}
	// Create a context for the lifetime of the stream
	streamCtx, cancel := context.WithCancel(context.Background())
	// Ensure streamCtx fires if the user-supplied context fires
	defer contextutil.NewLink(ctx, cancel).Stop()
	// Grab the stream
	stream, err := c.client.Upload(streamCtx)
	if err != nil {
		return err
	}
	defer cancel()
	// Send the path
	err = stream.Send(jobrpc.FileChunkFromPathAndID(path, id))
	// Handle any errors
	if err != nil {
		stream.CloseAndRecv() // Ignore any errors
		if err == io.EOF {
			err = metadataToError(stream.Trailer())
			if err == nil {
				err = errors.New("stream closed unexpectedly")
			}
		}
		return err
	}
	// Send the contents
	var buf bytes.Buffer
	var sendErr error
	for err == nil && sendErr == nil {
		_, err = io.CopyN(&buf, contents, defaultChunkSize)
		if err == nil || err == io.EOF {
			sendErr = stream.Send(jobrpc.FileChunkFromData(buf.Bytes()))
			buf.Reset()
		}
	}
	err = sendErr
	// Close the stream
	_, closeErr := stream.CloseAndRecv()
	if err == nil {
		err = metadataToError(stream.Trailer())
		if err == nil {
			err = closeErr
		}
	}
	return err
}

// Status returns the status of the job with the given ID.
func (c *Client) Status(ctx context.Context, id job.ID) (job.Status, error) {
	// Sanity check
	if err := c.isNilOrClosed(); err != nil {
		return nil, err
	}
	// Hand off to the server
	st, err := c.client.Status(ctx, jobrpc.FromID(id))
	if err != nil {
		return nil, err
	}
	// Convert the return value
	return jobrpc.ToStatus(st)
}

// Info returns information about the task with the given job ID and value n.
func (c *Client) Info(ctx context.Context, id job.ID, n int64) (job.Info, error) {
	// Sanity check
	if err := c.isNilOrClosed(); err != nil {
		return nil, err
	}
	// Hand off to the server
	inf, err := c.client.Info(ctx, jobrpc.FromIDAndValue(id, n))
	if err != nil {
		return nil, err
	}
	// Convert the return value
	return jobrpc.ToInfo(inf)
}

// List returns a slice containing the IDs of all jobs.
func (c *Client) List(ctx context.Context) ([]job.ID, error) {
	// Sanity check
	if err := c.isNilOrClosed(); err != nil {
		return nil, err
	}
	// Hand off to the server
	L, err := c.client.List(ctx, &emptypb.Empty{})
	if err != nil {
		return nil, err
	}
	// Convert the return value
	return jobrpc.ToIDs(L)
}

// ListWithPriority returns a slice containing the IDs of all jobs with the given priority level.
func (c *Client) ListWithPriority(ctx context.Context, level job.Priority) ([]job.ID, error) {
	// Sanity check
	if err := c.isNilOrClosed(); err != nil {
		return nil, err
	}
	// Hand off to the server
	L, err := c.client.ListWithPriority(ctx, jobrpc.FromPriority(level))
	if err != nil {
		return nil, err
	}
	// Convert the return value
	return jobrpc.ToIDs(L)
}

// Delete deletes the job with given ID.
func (c *Client) Delete(ctx context.Context, id job.ID) error {
	// Sanity check
	if err := c.isNilOrClosed(); err != nil {
		return err
	}
	// Hand off to the server
	_, err := c.client.Delete(ctx, jobrpc.FromID(id))
	return err
}

// Describe returns the submission data for the job with the given ID.
func (c *Client) Describe(ctx context.Context, id job.ID) (*job.Specification, error) {
	// Sanity check
	if err := c.isNilOrClosed(); err != nil {
		return nil, err
	}
	// Hand off to the server
	spec, err := c.client.Describe(ctx, jobrpc.FromID(id))
	if err != nil {
		return nil, err
	}
	// Convert the return value
	return jobrpc.ToSpecification(spec)
}

// Next returns a new task, if one is available. It provides the metadata mt to the job server.
func (c *Client) Next(ctx context.Context, mt job.Metadata) (job.Task, error) {
	// Sanity check
	if err := c.isNilOrClosed(); err != nil {
		return nil, err
	}
	// Convert the metadata
	metadata, err := jobrpc.FromWorkerMetadata(mt)
	if err != nil {
		return nil, err
	}
	// Hand off to the server
	t, err := c.client.Next(ctx, metadata)
	if err != nil {
		return nil, err
	}
	// Convert the return value
	return jobrpc.ToTask(t)
}

// Download downloads a file with the given path. This should be present in the manifest for the job with the given ID.
func (c *Client) Download(ctx context.Context, id job.ID, path string) (job.Reader, error) {
	// Sanity check
	if err := c.isNilOrClosed(); err != nil {
		return nil, err
	}
	// Create a context for the lifetime of the stream
	streamCtx, cancel := context.WithCancel(context.Background())
	// Ensure streamCtx fires if the user-supplied context fires
	defer contextutil.NewLink(ctx, cancel).Stop()
	// Request the stream
	stream, err := c.client.Download(streamCtx, jobrpc.FromFilePathAndID(path, id))
	if err != nil {
		return nil, err
	}
	return newStreamReader(stream, cancel), nil
}

// Success indicates that the task t has succeeded.
func (c *Client) Success(ctx context.Context, t job.Task) error {
	// Sanity checks
	if err := c.isNilOrClosed(); err != nil {
		return err
	} else if t == nil {
		return jerrors.ErrNilTask
	}
	// Convert the task
	task, err := jobrpc.FromTask(t)
	if err != nil {
		return err
	}
	// Hand off to the server
	_, err = c.client.Success(ctx, task)
	return err
}

// Error indicates that the task t has failed and should be retried.
func (c *Client) Error(ctx context.Context, t job.Task) error {
	// Sanity checks
	if err := c.isNilOrClosed(); err != nil {
		return err
	} else if t == nil {
		return jerrors.ErrNilTask
	}
	// Convert the task
	task, err := jobrpc.FromTask(t)
	if err != nil {
		return err
	}
	// Hand off to the server
	_, err = c.client.Error(ctx, task)
	return err
}

// Fatal indicates that the task t has failed and should not be requeued.
func (c *Client) Fatal(ctx context.Context, t job.Task) error {
	// Sanity checks
	if err := c.isNilOrClosed(); err != nil {
		return err
	} else if t == nil {
		return jerrors.ErrNilTask
	}
	// Convert the task
	task, err := jobrpc.FromTask(t)
	if err != nil {
		return err
	}
	// Hand off to the server
	_, err = c.client.Fatal(ctx, task)
	return err
}

// Requeue indicates that the task t should be requeued, without incrementing the number of failures.
func (c *Client) Requeue(ctx context.Context, t job.Task) error {
	// Sanity checks
	if err := c.isNilOrClosed(); err != nil {
		return err
	} else if t == nil {
		return jerrors.ErrNilTask
	}
	// Convert the task
	task, err := jobrpc.FromTask(t)
	if err != nil {
		return err
	}
	// Hand off to the server
	_, err = c.client.Requeue(ctx, task)
	return err
}

// Heartbeat sends a message to the job server indicating that the worker is still alive and is processing the task t. A nil value for t indicates that no task is being processed. Heartbeat returns true if task processing should continue and false if the current task should be cancelled.
func (c *Client) Heartbeat(ctx context.Context, t job.Task) (bool, error) {
	// Sanity checks
	if err := c.isNilOrClosed(); err != nil {
		return false, err
	}
	// Convert the task. The nil job.Task is represented by a *Task with the ID job.NilTaskID.
	task := &jobrpc.Task{Id: &jobrpc.TaskID{Id: job.NilTaskID.String()}}
	if t != nil {
		var err error
		task, err = jobrpc.FromTask(t)
		if err != nil {
			return false, err
		}
	}
	// Hand off to the server
	hr, err := c.client.Heartbeat(ctx, task)
	if err != nil {
		return false, err
	}
	// Convert the return value
	return jobrpc.ToBoolean(hr)
}
