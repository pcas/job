// Server handles connections from a jobdb client.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package jobdb

import (
	"bitbucket.org/pcas/job"
	"bitbucket.org/pcas/job/jobdb/internal/jobrpc"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/grpcmetrics"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/grpcutil/grpclog"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/protobuf/types/known/emptypb"
	"io"
	"time"

	// Make the compressor available
	_ "bitbucket.org/pcastools/grpcutil/grpcsnappy"
)

// The default ports that the jobdb server listens on.
const (
	DefaultTCPPort = 12360
	DefaultWSPort  = 80
)

// jobdbServer implements the tasks on the gRPC server.
type jobdbServer struct {
	jobrpc.UnimplementedJobdServer
	log.BasicLogable
	metrics.BasicMetricsable
	s job.Storage // the underlying inmem job storage system
}

// Server handles client communication.
type Server struct {
	log.SetLoggerer
	metrics.SetMetricser
	grpcutil.Server
}

/////////////////////////////////////////////////////////////////////////
// jobdbServer functions
/////////////////////////////////////////////////////////////////////////

// Submit submits a job.
func (s *jobdbServer) Submit(ctx context.Context, sp *jobrpc.Specification) (*jobrpc.ID, error) {
	if sp == nil {
		return &jobrpc.ID{}, errors.New("nil job specification in Submit")
	}
	// Convert the specification
	spec, err := jobrpc.ToSpecification(sp)
	if err != nil {
		return &jobrpc.ID{}, err
	}
	// Hand off to the in-memory client
	id, err := s.s.Submit(ctx, spec)
	if err != nil {
		return &jobrpc.ID{}, err
	}
	// Convert the return value
	return jobrpc.FromID(id), nil
}

// Upload uploads a file.
func (s *jobdbServer) Upload(stream jobrpc.Jobd_UploadServer) error {
	if stream == nil {
		return errors.New("nil stream in Upload")
	}
	// The first chunk should contain the path and ID
	chunk, delayedErr := fcRecvAndExamineTrailer(stream)
	if chunk == nil {
		if delayedErr != nil {
			return delayedErr
		}
		return errors.New("unexpected empty stream")
	}
	p, id, err := jobrpc.ToFilePathAndID(chunk.GetPathId())
	if err != nil {
		return err
	}
	// Wrap up the stream as an io.ReadCloser
	r := newBasicReader(stream)
	// Hand off to the client
	if err := s.s.Upload(stream.Context(), id, p, r); err != nil {
		return err
	}
	// Close the stream
	return stream.SendAndClose(&emptypb.Empty{})
}

// Status returns the status of a job
func (s *jobdbServer) Status(ctx context.Context, id *jobrpc.ID) (*jobrpc.StatusResponse, error) {
	if id == nil {
		return &jobrpc.StatusResponse{}, errors.New("nil ID specification in Status")
	}
	// Convert the ID
	j, err := jobrpc.ToID(id)
	if err != nil {
		return &jobrpc.StatusResponse{}, err
	}
	// Hand off to the in-memory client
	status, err := s.s.Status(ctx, j)
	if err != nil {
		return &jobrpc.StatusResponse{}, err
	}
	// Convert the return value
	return jobrpc.FromStatus(status)
}

// Info returns information about a task.
func (s *jobdbServer) Info(ctx context.Context, idv *jobrpc.IDAndValue) (*jobrpc.InfoResponse, error) {
	if idv == nil {
		return &jobrpc.InfoResponse{}, errors.New("nil ID and value in Info")
	}
	// Convert the ID and value
	id, value, err := jobrpc.ToIDAndValue(idv)
	if err != nil {
		return &jobrpc.InfoResponse{}, err
	}
	// Hand off to the in-memory client
	info, err := s.s.Info(ctx, id, value)
	if err != nil {
		return &jobrpc.InfoResponse{}, err
	}
	// Convert the return value
	return jobrpc.FromInfo(info)
}

// List returns the IDs of all jobs.
func (s *jobdbServer) List(ctx context.Context, _ *emptypb.Empty) (*jobrpc.ListResponse, error) {
	// Hand off to the in-memory client
	L, err := s.s.List(ctx)
	if err != nil {
		return &jobrpc.ListResponse{}, err
	}
	// Convert the return value
	return jobrpc.FromIDs(L)
}

// ListWithPriority returns the IDs of all jobs with the given priority.
func (s *jobdbServer) ListWithPriority(ctx context.Context, pr *jobrpc.Priority) (*jobrpc.ListResponse, error) {
	if pr == nil {
		return &jobrpc.ListResponse{}, errors.New("nil priority specification in ListWithPriority")
	}
	// Convert the priority
	priority, err := jobrpc.ToPriority(pr)
	if err != nil {
		return &jobrpc.ListResponse{}, err
	}
	// Hand off to the in-memory client
	L, err := s.s.ListWithPriority(ctx, priority)
	if err != nil {
		return &jobrpc.ListResponse{}, err
	}
	// Convert the return value
	return jobrpc.FromIDs(L)
}

// Delete deletes the job.
func (s *jobdbServer) Delete(ctx context.Context, id *jobrpc.ID) (*emptypb.Empty, error) {
	if id == nil {
		return &emptypb.Empty{}, errors.New("nil ID specification in Delete")
	}
	// Convert the ID
	j, err := jobrpc.ToID(id)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	// Hand off to the in-memory client
	return &emptypb.Empty{}, s.s.Delete(ctx, j)
}

// Describe returns the submission data for a job.
func (s *jobdbServer) Describe(ctx context.Context, id *jobrpc.ID) (*jobrpc.Specification, error) {
	if id == nil {
		return &jobrpc.Specification{}, errors.New("nil ID specification in Describe")
	}
	// Convert the ID
	j, err := jobrpc.ToID(id)
	if err != nil {
		return &jobrpc.Specification{}, err
	}
	// Hand off to the in-memory client
	spec, err := s.s.Describe(ctx, j)
	if err != nil {
		return &jobrpc.Specification{}, err
	}
	// Convert the return value
	return jobrpc.FromSpecification(spec)
}

// Next returns the next entry in a range.
func (s *jobdbServer) Next(ctx context.Context, mt *jobrpc.WorkerMetadata) (*jobrpc.Task, error) {
	if mt == nil {
		return &jobrpc.Task{}, errors.New("nil metadata specification in Next")
	}
	// Convert the metadata
	metadata, err := jobrpc.ToWorkerMetadata(mt)
	if err != nil {
		return &jobrpc.Task{}, err
	}
	// Hand off to the in-memory worker
	t, err := s.s.Next(ctx, metadata)
	if err != nil {
		return &jobrpc.Task{}, err
	}
	// Convert the return value
	return jobrpc.FromTask(t)
}

// Download downloads a file.
func (s *jobdbServer) Download(req *jobrpc.FilePathAndID, stream jobrpc.Jobd_DownloadServer) error {
	// Sanity checks
	if req == nil {
		return errors.New("nil request in Download")
	} else if stream == nil {
		return errors.New("nil stream in Download")
	}
	// Convert the path and job ID
	p, id, err := jobrpc.ToFilePathAndID(req)
	if err != nil {
		return err
	}
	// Request the file
	r, err := s.s.Download(stream.Context(), id, p)
	if err != nil {
		return err
	}
	// Start a background worker watching for the context to fire
	doneC := make(chan struct{})
	defer close(doneC)
	go func(doneC <-chan struct{}) {
		select {
		case <-stream.Context().Done():
			r.SetReadDeadline(time.Now()) // Ignore any errors
		case <-doneC:
		}
	}(doneC)
	// Break the contents of r into chunks and send them over the stream
	ok := true
	for ok && err == nil {
		chunk := make([]byte, defaultChunkSize)
		// Try to read a chunk from r
		n, err := r.Read(chunk)
		for err == nil && n < len(chunk) {
			var m int
			m, err = r.Read(chunk[n:])
			n += m
		}
		// Handle any error
		if err == io.EOF {
			// We zero out the error and finish iterating
			err = nil
			ok = false
		} else if err != nil {
			return err
		}
		err = stream.Send(jobrpc.FileChunkFromData(chunk[:n]))
	}
	return err
}

// Success indicates that the task has succeeded.
func (s *jobdbServer) Success(ctx context.Context, t *jobrpc.Task) (*emptypb.Empty, error) {
	if t == nil {
		return &emptypb.Empty{}, errors.New("nil task specification in Success")
	}
	// Convert the task
	task, err := jobrpc.ToTask(t)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	// Hand off to the in-memory worker
	return &emptypb.Empty{}, s.s.Success(ctx, task)
}

// Error indicates that the task has failed and should be retried.
func (s *jobdbServer) Error(ctx context.Context, t *jobrpc.Task) (*emptypb.Empty, error) {
	if t == nil {
		return &emptypb.Empty{}, errors.New("nil task specification in Error")
	}
	// Convert the task
	task, err := jobrpc.ToTask(t)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	// Hand off to the in-memory worker
	return &emptypb.Empty{}, s.s.Error(ctx, task)
}

// Requeue indicates that the task should be retried, without incrementing the number of failures.
func (s *jobdbServer) Requeue(ctx context.Context, t *jobrpc.Task) (*emptypb.Empty, error) {
	if t == nil {
		return &emptypb.Empty{}, errors.New("nil task specification in Requeue")
	}
	// Convert the task
	task, err := jobrpc.ToTask(t)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	// Hand off to the in-memory worker
	return &emptypb.Empty{}, s.s.Requeue(ctx, task)
}

// Fatal indicates that the task has failed and should not be retried.
func (s *jobdbServer) Fatal(ctx context.Context, t *jobrpc.Task) (*emptypb.Empty, error) {
	if t == nil {
		return &emptypb.Empty{}, errors.New("nil task specification in Fatal")
	}
	// Convert the task
	task, err := jobrpc.ToTask(t)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	// Hand off to the in-memory worker
	return &emptypb.Empty{}, s.s.Fatal(ctx, task)
}

// Heartbeat sends a heartbeat.
func (s *jobdbServer) Heartbeat(ctx context.Context, t *jobrpc.Task) (*jobrpc.HeartbeatResponse, error) {
	if t == nil {
		return &jobrpc.HeartbeatResponse{}, errors.New("nil task specification in Heartbeat")
	}
	// Convert the task
	task, err := jobrpc.ToTask(t)
	if err != nil {
		return &jobrpc.HeartbeatResponse{}, err
	}
	// Hand off to the in-memory worker
	carryOn, err := s.s.Heartbeat(ctx, task)
	if err != nil {
		return &jobrpc.HeartbeatResponse{}, err
	}
	// Convert the return value
	return jobrpc.FromBoolean(carryOn), nil
}

/////////////////////////////////////////////////////////////////////////
// Server functions
/////////////////////////////////////////////////////////////////////////

// NewServer returns a new jobdb server.
func NewServer(options ...Option) (*Server, error) {
	// Parse the options
	opts, err := parseOptions(options...)
	if err != nil {
		return nil, err
	}
	// Create the new server instance
	m := &jobdbServer{s: opts.Storage}
	// Create the gRPC options
	serverOptions := []grpc.ServerOption{
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			unaryServerErrorInterceptor,
			grpclog.UnaryServerInterceptor(m.Log()),
			grpcmetrics.UnaryServerInterceptor(m.Metrics()),
		)),
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			streamServerErrorInterceptor,
			grpclog.StreamServerInterceptor(m.Log()),
			grpcmetrics.StreamServerInterceptor(m.Metrics()),
		)),
	}
	// Add the SSL credentials to the gRPC options
	if len(opts.SSLKey) != 0 {
		creds, err := grpcutil.NewServerTLS(opts.SSLCert, opts.SSLKey)
		if err != nil {
			return nil, err
		}
		serverOptions = append(serverOptions, grpc.Creds(creds))
	}
	// Create the gRPC server and register our implementation
	s := grpc.NewServer(serverOptions...)
	jobrpc.RegisterJobdServer(s, m)
	// Bind the health data
	grpc_health_v1.RegisterHealthServer(s, health.NewServer())
	// Wrap and return the server
	return &Server{
		SetLoggerer:  m,
		SetMetricser: m,
		Server:       s,
	}, nil
}
