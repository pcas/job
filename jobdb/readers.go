// Readers provides a job.Reader and an io.Reader that wrap gRPC streams of file chunks.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package jobdb

import (
	"bitbucket.org/pcas/job"
	"bitbucket.org/pcas/job/jobdb/internal/jobrpc"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/timeutil"
	"context"
	"errors"
	"google.golang.org/grpc/metadata"
	"io"
	"sync"
	"time"
)

// trailer is an interface satisfied by anything with trailer metadata.
type trailerer interface {
	Trailer() metadata.MD
}

// fileChunkRecver is an interface satisfied by a stream of file chunks.
type fileChunkRecver interface {
	Recv() (*jobrpc.FileChunk, error)
}

// streamReader wraps a stream of file chunks. It implements job.Reader.
type streamReader struct {
	stream   fileChunkRecver // The underlying stream
	c        *timeutil.C     // The timeout worker
	buf      []byte          // The current chunk
	err      error           // The error on read (if any)
	m        sync.RWMutex    // Mutex protects the following...
	isClosed bool            // Have we closed?
}

// Sanity checks
var _ io.ReadCloser = &streamReader{}

// basicReader wraps a stream of file chunks. It implements io.Reader.
type basicReader struct {
	stream fileChunkRecver // The underlying stream
	buf    []byte          // The current chunk
	err    error           // The error on read (if any)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// fcRecvAndExamineTrailer attempts to read a *FileChunk from stream.  On error, if the error is io.EOF it reads the trailer metadata from stream, if it exists, and if an error e is found there, replaces io.EOF with e.
func fcRecvAndExamineTrailer(stream fileChunkRecver) (*jobrpc.FileChunk, error) {
	chunk, err := stream.Recv()
	if err != nil {
		if err == io.EOF {
			// Read the error from the trailer metadata, if possible
			if tr, ok := stream.(trailerer); ok {
				if trErr := metadataToError(tr.Trailer()); trErr != nil {
					err = trErr
				}
			}
		}
	}
	return chunk, grpcutil.ConvertError(err)
}

/////////////////////////////////////////////////////////////////////////
// streamReader functions
/////////////////////////////////////////////////////////////////////////

// newStreamReader returns a new fs.Reader. Here err is an optional delayed error. Assumes that the initial chunk (containing the file name) has already been read successfully.
func newStreamReader(stream fileChunkRecver, cancel context.CancelFunc) job.Reader {
	return &streamReader{
		stream: stream,
		c:      timeutil.NewCancellable(cancel),
	}
}

// Close closes the file for reading.
func (r *streamReader) Close() error {
	// Acquire a write lock
	r.m.Lock()
	defer r.m.Unlock()
	// Is there anything to do?
	if !r.isClosed {
		// Mark us as closed
		r.isClosed = true
		// Ask the timeout worker to exit
		r.c.Stop()
	}
	// Return any error (except for io.EOF)
	if r.err != io.EOF {
		return r.err
	}
	return nil
}

// Read reads up to len(b) bytes into b. It returns the number of bytes read and any error encountered.
func (r *streamReader) Read(b []byte) (int, error) {
	// Acquire a read lock
	r.m.RLock()
	defer r.m.RUnlock()
	// Sanity check
	if r.isClosed {
		return 0, errors.New("reader closed")
	}
	// Refill the buffer, if necessary
	for len(r.buf) == 0 {
		// Do we have a delayed error?
		if r.err != nil {
			return 0, r.err
		}
		// Read in the next chunk
		var chunk *jobrpc.FileChunk
		if err := r.c.WithCancel(func() (err error) {
			chunk, err = fcRecvAndExamineTrailer(r.stream)
			return
		}); err != nil {
			r.err = err
		}
		if chunk != nil {
			r.buf = chunk.GetData()
		}
	}
	// Copy the buffer to the slice
	n := copy(b, r.buf)
	r.buf = r.buf[n:]
	return n, nil
}

// SetReadDeadline sets the deadline for future Read calls and any currently-blocked Read call. A zero value for t means Read will not time out.
func (r *streamReader) SetReadDeadline(t time.Time) error {
	// Acquire a read lock
	r.m.RLock()
	defer r.m.RUnlock()
	// Sanity check
	if r.isClosed {
		return errors.New("reader closed")
	}
	// Update the timeout
	return r.c.SetDeadline(t)
}

// SetDeadline is an alias for SetReadDeadline.
func (r *streamReader) SetDeadline(t time.Time) error {
	return r.SetReadDeadline(t)
}

/////////////////////////////////////////////////////////////////////////
// basicReader functions
/////////////////////////////////////////////////////////////////////////

// newBasicReader returns a new io.Reader that wraps stream.
func newBasicReader(stream fileChunkRecver) io.Reader {
	return &basicReader{
		stream: stream,
	}
}

// Read reads up to len(b) bytes into b. It returns the number of bytes read and any error encountered.
func (r *basicReader) Read(b []byte) (int, error) {
	// Refill the buffer, if necessary
	for len(r.buf) == 0 {
		// Do we have a delayed error?
		if r.err != nil {
			return 0, r.err
		}
		// Read in the next chunk
		chunk, err := fcRecvAndExamineTrailer(r.stream)
		if err != nil {
			r.err = err
		}
		if chunk != nil {
			r.buf = chunk.GetData()
		}
	}
	// Copy the buffer to the slice
	n := copy(b, r.buf)
	r.buf = r.buf[n:]
	return n, nil
}
