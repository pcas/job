// Worker defines the WorkerStorage interface for the pcas job scheduling system.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package job

import (
	"bitbucket.org/pcastools/ulid"
	"context"
	"io"
	"time"
)

// TaskID represents a unique identifier for a task
type TaskID ulid.ULID

// NilTaskID is the special ID that represents a non-existent task
var NilTaskID = TaskID(ulid.Nil)

// Task represents a task
type Task interface {
	// Name is the name of the job to which this task belongs.
	Name() string
	// JobID is the ID of the job.
	JobID() ID
	// Manifest records the files and directories uploaded with the job.
	Manifest() Manifest
	// Script is the file in Manifest that is run for each task in the job.
	Script() string
	// WorkingDir is a directory in Manifest, either explicitly or implicitly (as a subpath).
	WorkingDir() string
	// Metadata records user-provided metadata.
	Metadata() map[string]string
	// Priority is the priority level of the job.
	Priority() Priority
	// ID is the ID of the task.
	ID() TaskID
	// Value is the value of the task.
	Value() int64
	// Deadline is the time at which the task will go stale.
	Deadline() time.Time
	// Failures is the number of times that this task has failed.
	Failures() int
}

// Metadata holds metadata about a worker
type Metadata struct {
	AppName  string // the name of the worker application
	Hostname string // the hostname of the machine on which the worker is running
}

// Reader is the interface describing a reader.
type Reader interface {
	io.ReadCloser
	// SetReadDeadline sets the deadline for future Read calls and any
	// currently-blocked Read call. A zero value for t means Read will not
	// time out.
	SetReadDeadline(t time.Time) error
	// SetDeadline is an alias for SetReadDeadline.
	SetDeadline(t time.Time) error
}

// WorkerStorage is the part of the interface satisfied by a job storage system that is needed by a worker.
type WorkerStorage interface {
	// Next returns a new task, if one is available. It provides the given metadata to the job server.
	Next(context.Context, Metadata) (Task, error)
	// Download downloads a file with the given path. This should be present in the manifest for the job with the given ID.
	Download(ctx context.Context, id ID, path string) (Reader, error)
	// Success indicates that the given task has succeeded.
	Success(context.Context, Task) error
	// Error indicates that the given task has failed and should be retried.
	Error(context.Context, Task) error
	// Requeue indicates that the given task should be requeued, without incrementing the number of failures.
	Requeue(context.Context, Task) error
	// Fatal indicates that the given task has failed and should not be requeued.
	Fatal(context.Context, Task) error
	// Heartbeat sends a message to the job server indicating that the worker is still alive and is processing the given task. A nil Task indicates that no task is being processed. Heartbeat returns true if task processing should continue and false if the current task should be cancelled.
	Heartbeat(context.Context, Task) (bool, error)
}

/////////////////////////////////////////////////////////////////////////
// TaskID functions
/////////////////////////////////////////////////////////////////////////

// String returns a string representation of the TaskID.
func (id TaskID) String() string {
	return ulid.ULID(id).String()
}
