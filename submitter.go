// Submitter defines the submitter and the SubmitterStorage interface for the pcas job scheduling system.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package job

import (
	"bitbucket.org/pcas/irange"
	"bitbucket.org/pcastools/ulid"
	"context"
	"io"
	"time"
)

// ID represents a unique identifier for a job
type ID ulid.ULID

// NilID is the special ID that represents a non-existent job
var NilID = ID(ulid.Nil)

// State records the state of a task.
type State uint8

// States of tasks
const (
	Uninitialised State = iota
	Pending
	Active
	Succeeded
	Failed
)

// Info describes a task.
type Info interface {
	Value() int64        // the value for this task
	State() State        // the state of this task
	AppName() string     // the application name provided by the client to which the task was assigned, or the empty string if there is no such client
	Hostname() string    // the hostname provided by the client to which the task was assigned, or the empty string if there is no such client
	Start() time.Time    // the time the task was assigned to a client, or the zero time if there is no such client
	Deadline() time.Time // the time at which this task will go stale, or the zero time if the task has not been assigned to a client
	Failures() int       // the number of times this task has failed
}

// Status describes the status of a job.
type Status interface {
	Pending() irange.Range   // The pending tasks
	Active() irange.Range    // The active tasks
	Succeeded() irange.Range // The tasks that succeeded
	Failed() irange.Range    // The tasks that failed
}

// SubmitterStorage is the part of the interface satisfied by a job storage system that is needed by a submitter.
type SubmitterStorage interface {
	// Submit submits a job, returning an ID which identifies that job. The job will become active when all files in the specified manifest have been uploaded. The storage system may discard a job if all such files are not uploaded after a certain length of time.
	Submit(context.Context, *Specification) (ID, error)
	// Upload uploads a file with the given path and contents. The path should be present in the manifest for the job with the given ID.
	Upload(ctx context.Context, id ID, path string, contents io.Reader) error
	// Status returns the status of the job with the given ID, or the error errors.ErrUnknownJob if there is no such active job.
	Status(context.Context, ID) (Status, error)
	// Info returns information about the task with the given value for the job with the given ID, or the error errors.ErrUnknownJob if there is no such active job.
	Info(context.Context, ID, int64) (Info, error)
	// List returns a slice containing the IDs of all active jobs.
	List(context.Context) ([]ID, error)
	// ListWithPriority returns a slice containing the IDs of all active jobs with the given priority level.
	ListWithPriority(context.Context, Priority) ([]ID, error)
	// Delete deletes the job with given ID, and returns the error errors.ErrUnknownJob if there is no such job.
	Delete(context.Context, ID) error
	// Describe returns the submission data for the job with the given ID, or the error errors.ErrUnknownJob if there is no such job.
	Describe(context.Context, ID) (*Specification, error)
}

/////////////////////////////////////////////////////////////////////////
// ID functions
/////////////////////////////////////////////////////////////////////////

// String returns a string representation of the ID.
func (id ID) String() string {
	return ulid.ULID(id).String()
}
